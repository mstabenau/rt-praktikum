%% Aufräumen
close all;
clear variables;
clc;

%% Parameterskript aufrufen, exportiert die Struktur par in den WS
aufgabe_4_1_parameters;

%% Weitere Parameter für die Berechnung der Referenztrajektorie
par.T   = 2.5;
par.k   = 120;          % 30 haben nicht gerreicht
Tsim    = par.T + 2;    % Längere Zeit
r       = 10;           % Interpolationfaktor

%% Start und Endwert
% Hier wird ein vierwertiges System betrachtet, da die Lösung für die
% ersten beiden Komponenten (y und v) bereits symbolisch aus Maxima
% bekannt ist
x0 = [0 0 -pi 0 -pi 0];
xT = zeros(1, 6);

%% Berechne Referenztrajektorie
[u_star, x_star, t] = aufgabe_4_1_reference(par, x0(3:6), xT(3:6));

%% Anmerkung zur Stellgrößenbeschränkung
% Mit den aktuellen Parametern verletzt die Lösung die Beschränkungen für
% y(t) und v(t). Die Wahl einer längeren Transitionszeit T sollte es
% möglich sein, diese Beschränkungen einzuhalten.
try
    assert(max(abs(u_star)) < 20, 'Beschleunigung größer als erlaubt');
    assert(max(abs(x_star(1,:))) < 0.7, 'Auslenkung größer als erlaubt');
    assert(max(abs(x_star(2,:))) < 2.2, 'Geschwindigkeit größer als erlaubt');
catch e
    warning(e.message);
end

%% Interpoliere das Ergebnis, um bessere Resultate zu erreichen
% Die Interpolation der 1-D-Lookup-Table ist nicht ausreichend, um die
% gewünschte Ruhelage zu erreichen. Selbst bei Wahl der besten
% (Cubic-Spline) Interpolation reichen die Abweichungen aus, um zu einem
% Verfehlen der Zielruhelage zu führen. Zusätzlich haben wir die Anzahl der
% Startgitterpunkte erhöht, um mehr exakte Werte des Solvers zu erreichen.
ti = interp(t, r);
u_star = interp1(t, u_star, ti, 'spline');

%% Zu den Aufgaben 4.2 c) und d)
% c) Die Ruhelage bleibt für t > T nicht erhalten. Stattdessen kippt das
% Pendel bei allen Extrapolationsmethoden um. Bei der
% Cubic-Spline-Extrapolation wächst außerdem der Eingang nach T
% quadratisch, wodurch der Wagen schnell seine erlaubten Grenzen verlässt.
% Die besten Resultate werden erzielt, wenn der Eingang nach T auf 0
% gesetzt wird.
% d) Der Kalman-Filter konvergiert für unsere Eingabe. Für abweichende
% Startwerte dauert es einige Sekunden, bis das Filter konvergiert. In
% diesem Fall kann es sinnvoll sein, einige Sekunden zu warten, bevor der
% Eingang aktiviert wird.