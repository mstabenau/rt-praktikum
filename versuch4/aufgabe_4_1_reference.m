function [u, x_extended, t] = aufgabe_4_1_reference(par, x0, xT, varargin)
    %% Berechne die Referenztrajektorie, um das DIP aufzuschwingen
    % par: verschiedene Parameter, unter anderem Systemparameter (Länge der
    %   Pendelarme, Massen, Reibparameter...), die Transitionszeit T und
    %   die Anzahl der Gitterpunkte k
    % x0: Startwert
    % xT: Endwert
    % plot: Boolean, ob die Trajektorien angezeigt werden sollen
    
    %% Initialwert für die offenen Parameter (p)
    p0 = zeros(1, 4);
    
    %% Extrahiere Parameter
    T = par.T;
    k = par.k;

    %% Schätzung der Lösungswerte in den Startgitterpunkten
    % Lineare Interpolation zwischen x0 und xT
    t =  T/(k - 1) * (0:k - 1);
    guess = @(t) x0 + (xT - x0)/T * t;
    
    %% Solver-Aufruf
    options = bvpset('stats', 'off','RelTol', 1e-6);
    solinit = bvpinit(t, guess, p0);
    sol = bvp5c(@(t, x, p)system_equations(t, x, p, par), ...
        @(xa, xb, ~) boundaries(xa, xb, x0, xT), ...
        solinit,options);
    
    %% Ausgabewerte berechnen
    t           = sol.x;
    [u, v, y]   = calculate_output_dynamic(t, sol.parameters, par.T);
    
    %% Führe die ursprünglichen Zustände zusammen
    x_extended  = [ y; v; sol.y];
    
    %% Plotten
    if nargin > 3 && varargin{1}
        plot_trajectories(x_extended, u, t);
    end
end

%% Systemgleichungen für die interne Dynamik
function dx = system_equations(t, x, p, par)
    %% Extrahiere Systemparameter aus der par-Struktur
    l1      = par.l1;
    l_S1    = par.l_S1;
    m_G1    = par.m_G1;
    J_G1xx  = par.J_G1xx;
    k_G1    = par.k_G1;
    l2      = par.l2;
    l_S2    = par.l_S2;
    m_G2    = par.m_G2;
    J_G2xx  = par.J_G2xx;
    k_G2    = par.k_G2;
    g       = par.g;

    %% Extrahiere Zustände
    phi_1   = x(1);
    omega_1 = x(2);
    phi_2   = x(3);
    omega_2 = x(4);
    
    %% Berechne Eingang
    u = calculate_output_dynamic(t, p, par.T);
    
    %% Berechne Ableitungen
    % Kopiert aus Maxima
    dphi1 = omega_1;

    ddphi1 = (l1*l_S2^2*m_G2^2*(cos(2*phi_2-phi_1)-cos(phi_1))+m_G2*(-2* ...
        l_S1*l_S2^2*m_G1*cos(phi_1)-2*J_G2xx*l1*cos(phi_1))-2*J_G2xx*...
        l_S1*m_G1*cos(phi_1))/(l1^2*l_S2^2*m_G2^2*(cos(2*phi_2-2*phi_1)...
        -1)+(-2*l_S1^2*l_S2^2*m_G1-2*J_G1xx*l_S2^2-2*J_G2xx*l1^2)*m_G2-2*...
        J_G2xx*l_S1^2*m_G1-2*J_G1xx*J_G2xx) * u-((l1^2*l_S2^2*m_G2^2*...
        omega_1^2*cos(phi_2-phi_1)+(l1*l_S2^3*m_G2^2+J_G2xx*l1*l_S2*...
        m_G2)*omega_2^2)*sin(phi_2-phi_1)+(-g*l1*l_S2^2*m_G2^2*sin(phi_2)...
        +k_G2*l1*l_S2*m_G2*omega_2-k_G2*l1*l_S2*m_G2*omega_1)*cos(phi_2-...
        phi_1)+(g*l1*l_S2^2*m_G2^2+(g*l_S1*l_S2^2*m_G1+J_G2xx*g*l1)*m_G2+...
        J_G2xx*g*l_S1*m_G1)*sin(phi_1)+(k_G2*l_S2^2*m_G2+J_G2xx*k_G2)*...
        omega_2+((-k_G2-k_G1)*l_S2^2*m_G2-J_G2xx*k_G2-J_G2xx*k_G1)*...
        omega_1)/(l1^2*l_S2^2*m_G2^2*cos(phi_2-phi_1)^2-l1^2*l_S2^2*...
        m_G2^2+(-l_S1^2*l_S2^2*m_G1-J_G1xx*l_S2^2-J_G2xx*l1^2)*m_G2-...
        J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);

    dphi2 = omega_2;

    ddphi2 = (m_G2*(l_S2*m_G1*(l1*l_S1*(cos(phi_2-2*phi_1)+cos(phi_2))-2*...
        l_S1^2*cos(phi_2))-2*J_G1xx*l_S2*cos(phi_2))+l1^2*l_S2*m_G2^2*...
        (cos(phi_2-2*phi_1)-cos(phi_2)))/(l1^2*l_S2^2*m_G2^2*(cos(2*phi_2...
        -2*phi_1)-1)+(-2*l_S1^2*l_S2^2*m_G1-2*J_G1xx*l_S2^2-2*J_G2xx*l1^2)...
        *m_G2-2*J_G2xx*l_S1^2*m_G1-2*J_G1xx*J_G2xx) * u+((l1^2*l_S2^2*...
        m_G2^2*omega_2^2*cos(phi_2-phi_1)+(l1^3*l_S2*m_G2^2+(l1*l_S1^2*...
        l_S2*m_G1+J_G1xx*l1*l_S2)*m_G2)*omega_1^2)*sin(phi_2-phi_1)+((g*...
        l1^2*l_S2*m_G2^2+g*l1*l_S1*l_S2*m_G1*m_G2)*sin(phi_1)+k_G2*l1*...
        l_S2*m_G2*omega_2+(-k_G2-k_G1)*l1*l_S2*m_G2*omega_1)*cos(phi_2-...
        phi_1)+((-g*l_S1^2*l_S2*m_G1-J_G1xx*g*l_S2)*m_G2-g*l1^2*l_S2*...
        m_G2^2)*sin(phi_2)+(k_G2*l1^2*m_G2+k_G2*l_S1^2*m_G1+J_G1xx*k_G2)...
        *omega_2+(-k_G2*l1^2*m_G2-k_G2*l_S1^2*m_G1-J_G1xx*k_G2)*omega_1)...
        /(l1^2*l_S2^2*m_G2^2*cos(phi_2-phi_1)^2-l1^2*l_S2^2*m_G2^2+...
        (-l_S1^2*l_S2^2*m_G1-J_G1xx*l_S2^2-J_G2xx*l1^2)*m_G2-J_G2xx*...
        l_S1^2*m_G1-J_G1xx*J_G2xx);
    
    %% Ausgabe
    dx = [dphi1; ddphi1; dphi2; ddphi2];
end

%% Berechnet den vorgebenen Eingang, die Geschwindigkeit und den Ausgang
function [u, v, y] = calculate_output_dynamic(t, p, T)
    %% Berechne Eingangsparameter
    p1 = p(1);
    p2 = p(2);
    p3 = p(3);
    p4 = p(4);
    
    % Kopiert aus Maxima
    a0 = -p3 - p1;
    a1 = -p4 - p2;
    
    %% Berechne Eingang
    % Kopiert aus Maxima
    u = -(25*pi^2*p4*cos((5*pi*t)/T))/T^2-(16*pi^2*p3*cos((4*pi*t)/T)) ...
        /T^2-(9*pi^2*p2*cos((3*pi*t)/T))/T^2-(4*pi^2*p1*cos((2*pi*t)/T))...
        /T^2-(pi^2*a1*cos((pi*t)/T))/T^2;
    
    v = -(5*pi*p4*sin((5*pi*t)/T))/T-(4*pi*p3*sin((4*pi*t)/T))/T-(3*pi*...
        p2*sin((3*pi*t)/T))/T-(2*pi*p1*sin((2*pi*t)/T))/T-(pi*a1*sin((pi*...
        t)/T))/T;
    
    y = p4*cos((5*pi*t)/T)+p3*cos((4*pi*t)/T)+p2*cos((3*pi*t)/T)+p1*cos(...
        (2*pi*t)/T)+a1*cos((pi*t)/T)+a0;
end

%% Grenzwerte: Anfangs- und Endzustände
function b = boundaries(xa, xb, x0, xT)
    b = [xa - x0'; xb - xT'];
end

%% Darstellung der Trajektorien
function plot_trajectories(x, u, t)
    %% Extrahiere Trajektorien
    ya      = x(1, :);
    dya     = x(2, :);
    phi     = x([3,5], :);
    dphi    = x([4,6], :);

    %% Plotten!
    figure('Name','Vorberechnete Trajektorie'); 
    subplot(3,2,1)
    plot(t,ya,'LineWidth',1.5) 
    title('Trajektorie der Wagenposition')
    xlabel('Zeit t [s]')
    ylabel('y*(t) [m]')
    grid on

    subplot(3,2,3)
    plot(t,phi,'LineWidth',1.5)
    title('Trajektorie der Winkel')
    xlabel('Zeit t [s]')
    ylabel('\phi*(t) [rad]')
    legend('\phi_1', '\phi_2');
    grid on

    subplot(3,2,5:6)
    plot(t,u,'LineWidth',1.5)
    title('Trajektorie der Vorsteuerung')
    xlabel('Zeit t [s]')
    ylabel('u*(t) [N]')
    grid on

    subplot(3,2,2)
    plot(t,dya,'LineWidth',1.5) 
    title('Trajektorie der Wagengeschwindigkeit')
    xlabel('Zeit t [s]')
    ylabel('v*(t) [m]')
    grid on

    subplot(3,2,4)
    plot(t,dphi,'LineWidth',1.5)
    title('Trajektorie der Winkelgeschwindigkeiten')
    xlabel('Zeit t [s]')
    ylabel('\omega* [rad]')
    grid on
    legend('\omega_1', '\omega_2');
end