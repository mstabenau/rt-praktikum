%% system parameters

%Pendelarm 1
par.l1 = 0.3230;
par.l_S1 = 0.1870;
par.m_G1 = 0.8810;
par.J_G1xx = 0.0134;
par.k_G1 = 0.0032;

%Pendelarm 2
par.l2 = 0.4800;
par.l_S2 = 0.1940;
par.m_G2 = 0.5510;
par.J_G2xx = 0.0208;
par.k_G2 = 0.0012;

par.g = 9.81;

%% kalman
xk0 = [0,0,-pi,0,-pi,0]';
P0 = eye(6) * 0.1;
R = eye(3) * 1e-3;
Q = eye(6) * 1e-6;