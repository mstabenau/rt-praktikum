function dip_sfunc(block) 

% level-2 Matlab M-file s-function
%
% -------------------------------------------------------------------------
% Beschreibung: Simulation des Turmdrehkrans 
% -------------------------------------------------------------------------
%
% inputs:   u = a_W              Wagenbeschleunigung
%
% states:   x(1)... y          Position des Wagens
%           x(2)... yp          Geschwindigkeit des Wagens
%           x(3)... phi1       Winkel Pendelarm 1
%           x(4)... phi1p       Winkelgeschwindigkeit Pendelarm 1
%           x(5)... phi2       Winkel Pendelarm 2
%           x(6)... phi2p     Winkelgeschwindigkeit Pendelarm 2
%
% output = states
%    
% -------------------------------------------------------------------------
% Abtastzeit (sample time): zeitkontinuierlich (continuous)
% -------------------------------------------------------------------------
setup(block);

% -------------------------------------------------------------------------
% Initialisierung des Simulationsobjektes block
% -------------------------------------------------------------------------

function setup(block)
  
  % Anzahl der Eingangs- und Ausgangsports
  block.NumInputPorts  = 1;
  block.NumOutputPorts = 1;
  
  % Anzahl der zeitkontinuierlichen Zustaende
  block.NumContStates = 6;

  % Anzahl der Parameter
  block.NumDialogPrms = 2;
  
  % Dimensionen der Eingangsports
  % Flag DirectFeedthrough kennzeichnet, ob ein Eingang direkt an einem
  % Ausgang auftritt, d.h. y=f(u)
  % Port 1:
  block.InputPort(1).Dimensions        = 1;
  block.InputPort(1).SamplingMode = 'Sample';
  block.InputPort(1).DirectFeedthrough = false;

  % Dimensionen der Ausgangsports  
  % Port 1:
  block.OutputPort(1).Dimensions       = 6;
  block.OutputPort(1).SamplingMode = 'Sample';  
  
  % Einstellen der Abtastzeit: [0 0] wird verwendet fuer die
  % zeitkontinuierliche Simulation.
  block.SampleTimes = [0 0];
  
  % ------------------------------------------------
  % NICHT VERAENDERN
  % ------------------------------------------------
  % 
  % Registrieren der einzelnen Methoden
  % Hier: InitializeConditions ... Initialisierung
  %       Outputs ...       Berechnung der Ausgaenge
  %       Derivatives ...   Berechnung der Zustaende
  %       Terminate ...     Konsistentes Beenden der Simulation

  block.RegBlockMethod('InitializeConditions',    @InitConditions); 
  block.RegBlockMethod('Outputs',                 @Output);  
  block.RegBlockMethod('Derivatives',             @Derivatives);  
  block.RegBlockMethod('Terminate',               @Terminate);


% -------------------------------------------------------------------------
% Setzen der Anfangsbedingungen der Zustaende
% -------------------------------------------------------------------------

function InitConditions(block)

  block.ContStates.Data = block.DialogPrm(1).Data;


% -------------------------------------------------------------------------
% Berechnen der Ausgaenge
% -------------------------------------------------------------------------

function Output(block)

  % Shortcut fuer den Zustand
  block.OutputPort(1).Data = block.ContStates.Data;
 
  

% -------------------------------------------------------------------------
% Berechnen der Zustaende
% -------------------------------------------------------------------------

function Derivatives(block)

  % Einlesen der Modellparameter (Parameter-Port 2)               
  par = block.DialogPrm(2).Data;
  
  % Shortcut fuer den Eingang
  a_W = block.InputPort(1).Data;
  
  % Shortcut fuer die Zustaende
  x = block.ContStates.Data;
  
  % Substitutionen
  y=x(1);
  yp=x(2);
  phi1=x(3);
  phi1p=x(4);
  phi2=x(5);
  phi2p=x(6);
  
  % Berechnung der Zustaende
  dx(1) = yp;
  dx(2) = a_W;
  dx(3) = phi1p;
  dx4num = -(-par.J_G2xx*phi1p*par.k_G1+(par.J_G2xx*phi2p-par.J_G2xx*phi1p)*par.k_G2+(par.J_G2xx*cos(phi1)*a_W...
            +par.g*par.J_G2xx*sin(phi1))*par.l_S1*par.m_G1+((par.g*sin(phi1)+cos(phi1)*a_W)*par.l_S1*par.l_S2^2*par.m_G1...
            +((phi2p-phi1p)*par.k_G2-phi1p*par.k_G1)*par.l_S2^2+(par.J_G2xx*par.l1*phi2p^2*sin(phi2-phi1)...
            +(par.l1*phi2p-par.l1*phi1p)*cos(phi2-phi1)*par.k_G2)*par.l_S2+par.J_G2xx*par.l1*cos(phi1)*a_W...
            +par.g*par.J_G2xx*par.l1*sin(phi1))*par.m_G2+(par.l1*phi2p^2*sin(phi2-phi1)*par.l_S2^3+(par.g*par.l1*sin(phi1)...
            -par.g*par.l1*sin(phi2)*cos(phi2-phi1)+par.l1^2*phi1p^2*cos(phi2-phi1)*sin(phi2-phi1)...
            +(par.l1*cos(phi1)-par.l1*cos(phi2)*cos(phi2-phi1))*a_W)*par.l_S2^2)*par.m_G2^2);
  dx4denum = ((par.l1^2*cos(phi2-phi1)^2-par.l1^2)*par.l_S2^2*par.m_G2^2+(-par.J_G2xx*par.l1^2-par.J_G1xx*par.l_S2^2-par.l_S1^2*par.l_S2^2*par.m_G1)*par.m_G2-par.J_G2xx*par.l_S1^2*par.m_G1-par.J_G1xx*par.J_G2xx);
  dx(4) = dx4num/dx4denum;
  dx(5) = phi2p;
  dx6num = ((par.J_G1xx*phi2p-par.J_G1xx*phi1p)*par.k_G2+(phi2p-phi1p)*par.k_G2*par.l_S1^2*par.m_G1...
           +(((par.l1*cos(phi1)*cos(phi2-phi1)*a_W+par.g*par.l1*sin(phi1)*cos(phi2-phi1))*par.l_S1...
           +(-cos(phi2)*a_W+par.l1*phi1p^2*sin(phi2-phi1)-par.g*sin(phi2))*par.l_S1^2)*par.l_S2*par.m_G1...
           +(-par.g*par.J_G1xx*sin(phi2)+par.J_G1xx*par.l1*phi1p^2*sin(phi2-phi1)-par.J_G1xx*cos(phi2)*a_W...
           -par.l1*phi1p*cos(phi2-phi1)*par.k_G1+(par.l1*phi2p-par.l1*phi1p)*cos(phi2-phi1)*par.k_G2)*par.l_S2...
           +(par.l1^2*phi2p-par.l1^2*phi1p)*par.k_G2)*par.m_G2+(par.l1^2*phi2p^2*cos(phi2-phi1)*sin(phi2-phi1)*par.l_S2^2+...
           (-par.g*par.l1^2*sin(phi2)+par.g*par.l1^2*sin(phi1)*cos(phi2-phi1)+par.l1^3*phi1p^2*sin(phi2-phi1)...
           +(par.l1^2*cos(phi1)*cos(phi2-phi1)-par.l1^2*cos(phi2))*a_W)*par.l_S2)*par.m_G2^2);
  dx6denum = ((par.l1^2*cos(phi2-phi1)^2-par.l1^2)*par.l_S2^2*par.m_G2^2+(-par.J_G2xx*par.l1^2-par.J_G1xx*par.l_S2^2-par.l_S1^2*par.l_S2^2*par.m_G1)*par.m_G2-par.J_G2xx*par.l_S1^2*par.m_G1-par.J_G1xx*par.J_G2xx);
  dx(6) = dx6num/dx6denum;
  
  % Schreiben auf Objekt block
  block.Derivatives.Data = dx;


% -------------------------------------------------------------------------
% Operationen am Ende der Simulation
% -------------------------------------------------------------------------

% Die function Terminate wird hier nicht verwendet,
% muss aber vorhanden sein!
function Terminate(block)

