% Finds a polynoms null values
function x = findzero(p, sstart)
    % Initialise
    i = 0;
    diff_p = polydiff(p);
    x_old = sstart;
    
    % First value (why is there no repeat until or something)
    x = x_old - polyval(p, x_old)/polyval(diff_p, x_old);
    
    % Use the newton method
    % Abort when value is exact enough or when it takes too long
    while ((abs(polyval(p, x)) > 0.000001) && (i < 10000))
        x_old = x;
        i = i + 1;
        x = x_old - polyval(p, x_old)/polyval(diff_p, x_old);
    end
end