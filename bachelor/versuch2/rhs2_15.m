%% Differential equation for 2.15
function xdot = rhs2_15(t,x,A)
    if (t < 0)
        u = 0;
    else
        u = 1;
    end
    
    xdot = A*x + [0;1]*u;
end