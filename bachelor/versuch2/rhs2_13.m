%% Non-linear function for vehicle axis
function xdot = rhs2_13(t,x,params)
    xdot = [
        x(2);
        (-params.c1*sinh((x(1)-u(t))/params.s10-1)+params.c2*sinh((x(3)-x(1))/params.s20-1)+params.d2*(x(4)-x(2)))/params.m1;
        x(4);
        -(params.c2*sinh((x(3)-x(1))/params.s20-1)+params.d2*(x(4)-x(2)))/params.m2;
    ];
end

function y = u(t)
    if ((t >= 2) && (t <= 3))
        y = 0.1*sin(pi*t);
    else
        y = 0;
    end
end