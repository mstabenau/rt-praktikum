clc
clear all
close all

%% 2.15a)
% Calculate eigenvalues for different values for a
a = -100:0.1:100;
ev = zeros(2,max(size(a)));

for i=1:max(size(a))
    ev(:,i) = eig([a(i),25;-25,0]);
end

plot(a, ev)

% The system is stable for a < 0. The system is stiff for a << 0 and a >> 0

%% 2.15 b)
% Parameters
tspan = -5:0.01:15;
a = -0.1:0.1:0.1;
x0 = [0;0];

% Solve
figure
for i = 1:3
    % Construct A
    A = [a(i),25;-25,0];
    
    % Solve using ODE45
    tic
    [t, y] = ode45(@(t,x) rhs2_15(t,x,A), tspan, x0);
    toc
    subplot(3,2, 2*i-1)
    plot(t, y)
    legend('x1','x2')
    
    % Solve using ODE15s
    tic
    [t, y] = ode15s(@(t,x) rhs2_15(t,x,A), tspan, x0);
    toc
    subplot(3,2, 2*i)
    plot(t, y)
    legend('x1','x2')
end

% ODE15s is slower this time by a factor of 2 or 3
% The results for both algorithms look the same