%% This is the full non-linear system as calculated in 1.7 d)
function xdot = rhs2_11(t,x,params, disturbed)
    xdot = [
        - (-u_GSM(t) + x(1)*params.R_GSM + params.k_GSM*x(3))/params.L_GSM;
        x(3)-x(4);
        -(params.c_GSMP*x(2) - params.d_GSMP*x(4) + (params.d_vGSM + params.d_GSMP)*x(3) - x(1)*params.k_GSM + params.d_cGSM)/params.J_GSM;
        (params.c_GSMP*x(2) - params.d_qP*x(4)^2 - (params.d_vP + params.d_GSMP)*x(4) + params.d_GSMP*x(3) - M_ext(t, disturbed))/params.J_P;
    ];
end

%% Input functions
function out = u_GSM(t)
    out = 12 - 2*heaviside(t-2) + 2*heaviside(t-5) - 4* heaviside(t-8) + 4*heaviside(t-13);
end


function out = M_ext(t, disturbed)
    if (disturbed)
        out = 0.05*heaviside(t-11);
    else
        out = 0;
    end
end