%%
clc
close all
clear all

%% 2.5a)
% Declare variables
A = [-2 0; 5 -3 ];
B_poly = [1, -2, -1, 2];

% Calculate p1*p2
p3 = conv(poly(A),B_poly)

%% 2.5 b)
polydiff([5,4,3,2,1])

%% 2.5 c)
findzero([2,3,4,5,6,-20],500)

%% 2.5 d)
p3_zero = findzero(p3,100)

%% 2.5 e)
roots(p3)

%% 2.5 f)
poly(A)

% The characteristic polynom is a Hurwitz-polynom, so all eigenvalues
% should be negative
eig(A)

%% 2.5 g)
polyvalm(p3, A)
matrix_poly(p3, A)