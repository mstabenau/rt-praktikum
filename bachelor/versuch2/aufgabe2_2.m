% Define matrix and vectors
A = [ 1 1 1; 5 4 -1; 24 19 -2];
B = [9;1;26];

% Find solution using inv
inv(A)*B

% Find solution using mldivide
mldivide(A,B)

% mldivide can find solutions for systems, when A is singular
% inv should only be used, when you can be sure that A is non-singular