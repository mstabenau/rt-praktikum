% Matlab would not let me use its own heaviside function, since the
% symbolic toolbox is not installed
function x = heaviside(t)
    x = t;
    x(x < 0) = 0;
    x(x > 0) = 1;
end