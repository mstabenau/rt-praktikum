%% Differential equation for 2.10
function xdot = rhs2_10(t,x)
    xdot = [
        -tanh(x(3))*x(1) - x(2)*x(3)^2 - exp(-2*x(3))*sin(t);
        x(1);
        x(2);
    ];
end