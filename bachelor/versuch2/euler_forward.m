function xout = euler_forward(A, B, u, x0, t)
    % Get dimensions
    x_dim = max(size(t));
    y_dim = max(size(x0));
    
    % Initialise state matrix
    xout = zeros(y_dim,x_dim);
    xout(:,1) = x0;
    
    % Start solving
    for i = 2:x_dim
        % Calculate next step
        h = t(i) - t(i-1);
        xout(:,i) = xout(:,i-1) + h*(A*xout(:,i-1) + B*u(i-1));
    end
end