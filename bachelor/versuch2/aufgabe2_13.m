clc
clear all
close all

%% 2.13a)
% Declare params
params.s10 = 0.4;
params.s20 = 0.6;
params.m1 = 100;
params.m2 = 1000;
params.c1 = 5000;
params.c2 = 100;
params.d2 = 1000;

% Declare variables
x0 = [params.s10;0;params.s10 + params.s20;0];
tspan = 0:0.01:6;

% Solve differential system
[t1, sol1] = ode45(@(t,x) rhs2_13(t,x,params), tspan, x0);

% Plot it
figure
plot(t1, sol1)
title('Non-linear system')
legend('s1','s1_dot','s2','s2_dot')

%% 2.13 b)
% Declare variables
A = [ 
    0 1 0 0;
    -(params.c1*params.s20 + params.c2*params.s10)/(params.m1*params.s10*params.s20), -params.d2/params.m1, params.c2/(params.m1*params.s20), params.d2/params.m1;
    0 0 0 1;
    params.c2/(params.m2*params.s20), params.d2/params.m2, -params.c2/(params.m2*params.s20), -params.d2/params.m2
];
B = [0; params.c1/(params.m1*params.s10); 0; 0];
%C = [0, 0, 1, 0];
C = eye(4);
D = 0;

% Generate input
u = zeros(size(tspan));
for i=1:max(size(tspan))
    if (tspan(i) >= 2) && (tspan(i) <= 3)
        u(i) = 0.1*sin(pi*tspan(i));
    end
end

% Create system
system = ss(A,B,C,D);

% Simulate system
[sol2, t2] = lsim(system,u,tspan);

% Plot it
figure
plot(t2, sol2)
title('Linearised system')
legend('s1','s1_dot','s2','s2_dot')