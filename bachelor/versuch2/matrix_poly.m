% Reimplementation of polyvalm
function x = matrix_poly(p, A)
    x = zeros(size(A));
    size_p = size(p);
    
    for i= 1:size_p(2)
        x = x +  p(1,i) * A^(size_p(2)- i);
    end
end