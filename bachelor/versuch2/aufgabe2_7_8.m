clear all
close all
clc

%% 2.7 a)
% Define system variables
A = [0 1; -6 -2];
B = [-1 ; 6];
C = [1, 0];
D = 0;
x0 = [0; 0];

% Create system
system = ss(A,B,C,D);

% Solve system
t = 0:0.01:5;
h1 = step(system, t);

% Display solution
figure
hold on
subplot(2,2,1)
plot(t,h1, 'red')

%% 2.7 b)
% Create discrete system
Ta = 0.1;
discrete = c2d(system, Ta, 'zoh');

% Solve
[h1d, t2] = step(discrete, 5);

% Display
subplot(2,2,2)
plot(t2,h1d, 'green')

% Display difference, resample h1d
ts = timeseries(h1d, t2);
ts_resampled = resample(ts, t, 'zoh');
h1d_resampled = ts_resampled.data;

hold on
plot(t, h1 - h1d_resampled, 'blue')

%% 2.7 c)
% Create system with lower sample time
Ta2 = 1;
discrete2 = c2d(system, Ta2, 'zoh');

% Solve
[h1d2, t3] = step(discrete2, 5);

% Display
subplot(2,2,3)
plot(t3, h1d2, 'green')

% Display difference
ts = timeseries(h1d2, t3);
ts_resampled = resample(ts, t, 'zoh');
h1d2_resampled = ts_resampled.data;

hold on
plot(t, h1 - h1d2_resampled, 'blue')

%% 2.8 a)
% Declare variables
h = 0.1;
t4 = 0:h:5;
u = ones(1, max(size(t4)));

% Use euler forward method
x = euler_forward(A, B, u, x0, t4);

%% 2.8 b)
% Resample solution from a) to match system
x1 = squeeze(transpose(x(1,:)));
ts = timeseries(x1,t4);
ts = resample(ts,t,'zoh');

% Plot it
subplot(2,2,4)
plot(t, ts.data, 'green')

% Plot difference to original system
hold on
plot(t, h1 - ts.data, 'blue')

%% 2.8 c)
% Plot old answer
figure
subplot(3,1,1)
plot(t4,x1, 'red')

% Calculate the same step response for a greater step size
h = 0.25;
t5 = 0:h:5;
x = euler_forward(A, B, u, x0, t5);
x1 = squeeze(transpose(x(1,:)));

% Plot it
hold on
subplot(3,1,2)
plot(t5,x1)

% Use an even greater step size
h = 0.5;
t6 = 0:h:5;
x = euler_forward(A, B, u, x0, t6);
x1 = squeeze(transpose(x(1,:)));

% Plot it
hold on
subplot(3,1,3)
plot(t6,x1)