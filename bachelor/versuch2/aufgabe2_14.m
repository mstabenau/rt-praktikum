clc
clear all
close all

%% 2.14
% Declare matrix
A = [
    -2 0 1;
    0 -1 5;
    0 0 -10^5
];
tspan = 0:0.01:10;
x0 = [0;0;0];

% Solve using ODE45
tic
[t1,y1] = ode45(@(t,x) rhs2_14(t,x,A), tspan, x0);
toc

% Solve using ODE15s
tic
[t2, y2] = ode15s(@(t,x) rhs2_14(t,x,A), tspan, x0);
toc

% Plot solutions
figure
subplot(1,2,1)
title('ODE45')
plot(t1,y1)
legend('x1','x2','x3')

subplot(1,2,2)
title('ODE15s')
plot(t2,y2)
legend('x1','x2','x3')