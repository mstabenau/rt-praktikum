% Clear
clc
clear all
close all

% Render sawtooth
figure
for i=1:100
    % Updating the title does not work unfortunately
    %string = strcat('Saegezahn-Approximation ', num2str(i), '. Ordnung');
    %title(string)
    swave(i,0:.01:5*pi,5);
    pause(.1);
end