clear all
clc
close all

%% 2.10
% Define variables
tspan = 0:0.01:20;
x0 = [10; 0; 1];
u = sin(tspan);

% Solve equation
[t, sol] = ode45(@rhs2_10, tspan, x0);

% Plot it
figure
plot(t, sol(:,1))