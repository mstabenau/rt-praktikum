clc
clear all
close all

%% 2.9 a)
% Define variables
h = 0.1;
tspan = 0:h:100;
x0 = 0.5;

% Solve differential equation
[t, sol] = ode45(@rhs, tspan,x0);

%% 2.9 b)
% Solve differential equation using euler forward method
sol_euler = zeros(length(tspan),1);
sol_euler(1) = x0;

for i=2:max(size(t))
    sol_euler(i,1) = sol_euler(i-1,1) + h*sin(sol_euler(i-1,1));
end

% Plot the two solutions
figure
plot(t,sol, 'red')
hold on
plot(tspan, sol_euler, 'green')

% Show mean difference
mean(abs(sol-sol_euler))

% Show maximum difference
max(abs(sol-sol_euler))