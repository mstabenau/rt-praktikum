%% Function for differential equation 2.9
function xdot = rhs(t,x)
    xdot = sin(x);
end