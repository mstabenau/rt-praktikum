% saw-tooth approximation
function out = swave(N,x,A)
    y = zeros(size(x));
    for i=1:N
        y = y + (-1)^(i+1) * (2*sin(i*x)/i);
    end
    y = A*y;

    plot(x,y)
    out = y;
end