function y = poly_diff(p)
    size_p = size(p);
    y = zeros(size_p);

    for i=2:size_p(2)
        y(i) = (size_p(2)-i+1) *p(i-1);
    end
end