%% Initialise all the values
% System parameters
params.M = 3.2;
params.m = 0.5502;
params.g = 9.8144;
params.J_S = 0.0168;
params.l = 0.4524;
params.c_w = 6.2;
params.c_p = 0.006;

% State of rest
x0 = [ 0, 0, 10, 0 ];

% State-space variables
A = [
    0, 0,                  1,                  0; 
    0, 0,                  0,                  1;
    0, 0.9929926303289659, -1.820515728022154, -0.00487775895325137;
    0, 29.92176916182627,  -5.04035091835975,  -0.1469811285284837
];
B = [ 0; 0; 0.2936315690358313; 0.8129598255418952 ];
C = eye(4);
D = [ 0; 0; 0; 0 ];