function aufgabe5_2_func( block )
    % Full non-linear system as L2 S-function for 5.2 b)
    setup(block);
end

function setup(block)
    % Ports
    block.NumInputPorts  = 1;
    block.NumOutputPorts = 1;

    % States
    block.NumContStates = 4;

    % Settings
    block.NumDialogPrms = 2;

    %Input
    block.InputPort(1).Dimensions        = 1;
    block.InputPort(1).SamplingMode = 'Sample';
    block.InputPort(1).DirectFeedthrough = false;
    
    % Output
    block.OutputPort(1).Dimensions       = 4;
    block.OutputPort(1).SamplingMode = 'Sample';
    
    % Sample time
    block.SampleTimes = [0 0];
    
    % Callbacks
    block.RegBlockMethod('InitializeConditions',    @InitConditions); 
    block.RegBlockMethod('Outputs',                 @Output);  
    block.RegBlockMethod('Derivatives',             @Derivatives);
end

function InitConditions(block)
    % Initialise with rest state (x0)
    block.ContStates.Data = block.DialogPrm(1).Data;
end

function Output(block)
    % Output
    block.OutputPort(1).Data = block.ContStates.Data;
end

function Derivatives(block)
    % Inputs
    F_w = block.InputPort(1).Data;
    
    % States and params
    x = block.ContStates.Data;
    params = block.DialogPrm(2).Data;
    
    % Fetch params to reduce the amount of typing
    M = params.M;
    m = params.m;
    g = params.g;
    J_S = params.J_S;
    l = params.l;
    c_w = params.c_w;
    c_p = params.c_p;
    
    F_w_nlin = 0;
    
    % Split the whole thing into multiple variables
    diff_x3_upper1 = (8*c_w*x(3) + 4*l*m*x(4)^2*sin(x(2)) + 8*F_w_nlin - 8*F_w)*J_S;
    diff_x3_upper2 = 2*c_w*l^2*m*x(3) + (l^3*m^2*x(4)^2 - 2*g*l^2*m^2*cos(x(2)))*sin(x(2));
    diff_x3_upper3 = 4*c_p*l*m*x(4)*cos(x(2)) + (2*F_w_nlin - 2*F_w)*l^2*m;
    diff_x3_lower = (8*J_S + 2*l^2*m)*M + 8*m*J_S + 2*l^2*m^2*(sin(x(2)))^2;

    diff_x4_upper1 = (2*g*l*m*sin(x(2)) - 4*c_p*x(4) )*M - 2*c_w*l*m*cos(x(2))*x(3); 
    diff_x4_upper2 = (2*g*l*m^2 - l^2*m^2*x(4)^2*cos(x(2)))*sin(x(2));
    diff_x4_upper3 = (2*F_w + 2*F_w_nlin)*l*m*cos(x(2)) - 4*c_p*m*x(4);
    diff_x4_lower = (4*J_S + l^2*m)*M + 4*m*J_S + l^2*m^2*(sin(x(2)))^2;
    
    % Output to derivatives
    block.Derivatives.Data = [
        x(3);
        x(4);
        -(diff_x3_upper1 + diff_x3_upper2 + diff_x3_upper3)/diff_x3_lower;
        (diff_x4_upper1 + diff_x4_upper2 + diff_x4_upper3)/diff_x4_lower
    ];
end