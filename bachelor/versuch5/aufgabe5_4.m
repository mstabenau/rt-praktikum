clc
clear all
close all

%% 5.4a)
aufgabe5_2_init;

% Overwrite C and D to only have x as the output
C = [ 1 , 0 , 0, 0 ];
D = 0;

% New initial state
x0 = [ 0; 0.00001; 0; 0 ];

% Init system and plot impulse response
system = ss(A,B,C,D);

figure
impulse(system)

%% 5.4 b)
g = tf(system);

figure
pzmap(g)

%% 5.4 c)
if (rank(obsv(A,C)) == max(size(A)))
    disp('Observable');
else
    disp('Not observable');
end

if (rank(ctrb(A,B)) == max(size(A)))
    disp('Controllable');
else
    disp('Not controllable');
end

%% 5.4 d)
% On paper

%% 5.4 e)
% Get target polynomial coefficients
s = tf('s');
(s+5)^7;
f = ans.num{1};

% Correct formatting for f
f = fliplr(f)';

% Define parameters
a = fliplr(g.den{1});
b = fliplr(g.num{1});

% Define sylvester matrix
R = [
    b(1) a(1) 0    0    0    0    0    0
    b(2) a(2) b(1) a(1) 0    0    0    0
    b(3) a(3) b(2) a(2) b(1) a(1) 0    0
    b(4) a(4) b(3) a(3) b(2) a(2) b(1) a(1)
    b(5) a(5) b(4) a(4) b(3) a(3) b(2) a(2)
    0    0    b(5) a(5) b(4) a(4) b(3) a(3)
    0    0    0    0    b(5) a(5) b(4) a(4)
    0    0    0    0    0    0    b(5) a(5)
];

% Calculate solution
coeffs = R^-1*f;

% Construct controller transfer function
gr = tf(1);
gr.num{1} = [ coeffs(7) coeffs(5) coeffs(3) coeffs(1) ];
gr.den{1} = [ coeffs(8) coeffs(6) coeffs(4) coeffs(2) ];

%% 5.4 f)
figure
impulse(gr)

%% 5.4 g)
% Get transfer function for k=1
T = 1/2;
gv = 1/(1+s/T);
t_ry_temp = minreal(g*gv/(1+g*gr));

% Calculate k
k = t_ry_temp.den{1}(9)/t_ry_temp.num{1}(9);
gv = k/(1+s/T);

% Get final transfer function
t_ry = k*t_ry_temp;

%% 5.4 h)
figure
step(t_ry)