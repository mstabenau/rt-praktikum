clc
clear all
close all
%% 5.3 b)
A = [ -1 2; 0 1 ];
B = [ 0; -1 ];
C = [ 1, 0 ];
D = 0;

g = tf(ss(A,B,C,D));
%% 5.3 e)
% Read coefficients from transfer function
[b , a] = tfdata(g);
b = fliplr(cell2mat(b));
a = fliplr(cell2mat(a));

% Define variables
b0 = b(1);
b1 = b(2);
b2 = b(3);

a0 = a(1);
a1 = a(2);
a2 = a(3);

% Define target eigenvalues
lambda1 = -1;
lambda2 = -2;
lambda3 = -3;

% Calculate f
f = conv([1 -lambda1],conv([1 -lambda2],[1 -lambda3]));
f = flipud(f');

% Define R
R = [
    b0, a0, 0,   0;
    b1, a1, b0, a0;
    b2, a2, b1, a1;
    0,   0, b2, a2
];

% Calculate resulting controller coefficients
coeffs = R^-1*f;
c0 = coeffs(2);
c1 = coeffs(4);
d0 = coeffs(1);
d1 = coeffs(3);

% Define transfer functions
s = tf('s');
g = (b0 + b1*s + b2*s^2)/(a0 + a1*s + a2*s^2);
gr = (d0 + d1*s)/(c0 + c1*s);
gv = -6/(c0 + c1*s); % Found -3 through trial-and-error

t_ry = g*gv/(1+gr*g);
t_ry = minreal(t_ry);

%% 5.3f)
% Plot transfer function
t = 0:0.01:10;
y = step(t_ry,t);

figure
plot(t,y)

%% 5.3g)
% Get transfer function
t_ru = minreal(t_ry / g);

% Calculate results
figure
u1 = zeros(size(t));
lsim(t_ru, u1, t);

figure
u2 = 0.5 * ones(size(t));
lsim(t_ru, u1,t);