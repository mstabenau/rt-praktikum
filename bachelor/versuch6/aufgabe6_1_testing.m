A = [0 4 2; 4 -2 4; 4 3 -6];
B = [1;2;4];
lambda = [ -1, -2, -3];

w_T = [ 0 0 1 ] * ctrb(A,B)^-1;

p = [1 -lambda(1)];
p = conv(p, [1 -lambda(2)]);
p = conv(p, [1 -lambda(3)]);
p = fliplr(p);

k_T_1 = w_T * (eye(3)*p(1) + A*p(2) + A^2*p(3) + A^3*p(4));

k_T = place(A,B,lambda);