function aufgabe6_2_func( block )
    % Reduced non-linear system for the controlled pendulum as L2 S-function
    setup(block);
end

function setup(block)
    % Ports
    block.NumInputPorts  = 1;
    block.NumOutputPorts = 1;

    % States
    block.NumContStates = 4;

    % Settings
    block.NumDialogPrms = 2;

    %Input
    block.InputPort(1).Dimensions        = 1;
    block.InputPort(1).SamplingMode = 'Sample';
    block.InputPort(1).DirectFeedthrough = false;
    
    % Output
    block.OutputPort(1).Dimensions       = 4;
    block.OutputPort(1).SamplingMode = 'Sample';
    
    % Sample time
    block.SampleTimes = [0 0];
    
    % Callbacks
    block.RegBlockMethod('InitializeConditions',    @InitConditions); 
    block.RegBlockMethod('Outputs',                 @Output);  
    block.RegBlockMethod('Derivatives',             @Derivatives);
end

function InitConditions(block)
    % Initialise with rest state (x0)
    block.ContStates.Data = block.DialogPrm(1).Data;
end

function Output(block)
    % Output
    block.OutputPort(1).Data = block.ContStates.Data;
end

function Derivatives(block)
    % Inputs
    u = block.InputPort(1).Data;
    
    % States and params
    x = block.ContStates.Data;
    params = block.DialogPrm(2).Data;
    
    % Fetch params to reduce the amount of typing
    m = params.m;
    g = params.g;
    J_S = params.J_S;
    l = params.l;
    c_p = params.c_p;
    
    % Split the long equation into multiple parts
    diff_4_upper = 2*m*cos(x(2))*u+2*g*l*m*sin(x(2))-4*c_p*x(4);
    diff_4_lower = 4*J_S+l^2*m;
    
    % Output to derivatives
    block.Derivatives.Data = [
        x(3);
        x(4);
        u;
        diff_4_upper/diff_4_lower
    ];
end