/* [wxMaxima batch file version 1] [ DO NOT EDIT BY HAND! ]*/
/* [ Created with wxMaxima version 13.04.2 ] */

/* [wxMaxima: title   start ]
controllable
   [wxMaxima: title   end   ] */

/* [wxMaxima: comment start ]
controllable: returns a list of the controllability matrix and its rank
   [wxMaxima: comment end   ] */

/* [wxMaxima: input   start ] */
controllable(A,B) := block([],
    ctrb: controllability_matrix(A,B),
    return([ctrb, rank(ctrb)])
)$
/* [wxMaxima: input   end   ] */

/* [wxMaxima: title   start ]
transform_rnf
   [wxMaxima: title   end   ] */

/* [wxMaxima: comment start ]
transform_rnf: transforms a system to its RNF.
   [wxMaxima: comment end   ] */

/* [wxMaxima: input   start ] */
defstruct(rnf(A,B,V,W,w_T));
transform_rnf(A,B) := block([],
    ctrb: invert(controllability_matrix(A,B)),
    w_T: ctrb[length(A)],
    W: matrix(w_T),
    for i: 1 thru length(A) - 1 do W: addrow(W, w_T.A^^i),
    V: invert(W),
    return(new(rnf(W.A.V,W.B,V,W,w_T)))
)$
/* [wxMaxima: input   end   ] */

/* [wxMaxima: title   start ]
acker
   [wxMaxima: title   end   ] */

/* [wxMaxima: comment start ]
acker: returns feedback vector
   [wxMaxima: comment end   ] */

/* [wxMaxima: input   start ] */
acker(A,B,lambda) := block([],
    ctrb: invert(controllability_matrix(A,B)),
    w_T: ctrb[length(A)],
    p: get_coeffs(lambda),
    k_T: w_T.sum(p[i+1]*A^^i,i,0,length(A)),
    return(k_T)
)$
/* [wxMaxima: input   end   ] */

/* [wxMaxima: title   start ]
get_coeffs
   [wxMaxima: title   end   ] */

/* [wxMaxima: comment start ]
get_coeffs: returns polynomial coefficients for a given list of solutions
   [wxMaxima: comment end   ] */

/* [wxMaxima: input   start ] */
get_coeffs(lambda) := block([],
    poly: 1,
    for i: 1 thru length(lambda) do poly: poly*(x - lambda[i]),
    poly: expand(poly),
    result: [],
    for i: 0 thru hipow(poly, x) do result: endcons(coeff(poly,x,i), result),
    return(result)
)$
/* [wxMaxima: input   end   ] */

/* [wxMaxima: title   start ]
place
   [wxMaxima: title   end   ] */

/* [wxMaxima: comment start ]
place: returns feedback vector, using the rnf
   [wxMaxima: comment end   ] */

/* [wxMaxima: input   start ] */
place(rnf,lambda) := block([],
    A_C: rnf@A,
    a_c: A_C[length(A_C)],
    p: get_coeffs(lambda),
    k_CT: [],
    for i:1 thru length(a_c) do k_CT: endcons(p[i] + a_c[i], k_CT),
    return(k_CT.W)
)$
/* [wxMaxima: input   end   ] */

/* Maxima can't load/batch files which end with a comment! */
"Created with wxMaxima"$
