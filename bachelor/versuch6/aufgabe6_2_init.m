%% Initialise all the values
% System parameters
params.M = 3.2;
params.m = 0.5502;
params.g = 9.8144;
params.J_S = 0.0168;
params.l = 0.4524;
params.c_w = 6.2;
params.c_p = 0.006;

% Initial states
x0 = [ 0, 0, 0, 0 ];

% State-space variables
A = [
    0 0      1 0;
    0 0      0 1;
    0 0      0 0;
    0 27.172 0 -0.13347
];

B = [ 0; 0; 1; 6.1198 ];

% Define C to observe position and angle
C = [ 1 0 0 0; 0 1 0 0];

% Target eigenvalues
lambda = [ -5, -6, -7, -6.5 ];

% Feedback vector
k_T = acker(A,B,lambda);

% Observer gain
lambda_observer = 3*lambda;
l = place(A',C',lambda_observer);

% Transform matrix, copied from maxima
W = [
    -0.036801,0.0060174,-1.8077*10^-4,2.9539*10^-5;
     0.0,8.0265*10^-4,-0.036801,0.0060134;
     0.0,0.1634,0.0,-1.0842*10^-19;
     0.0,0.0,0.0,0.1634
];
V = inv(W);

% Input parameters for the controller
T = 3;
zt=W*A*V;
zt=zt(4,:);

% Final value for the carts position
r = 3;