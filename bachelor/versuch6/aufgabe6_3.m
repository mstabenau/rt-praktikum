%% Aufgabe 6.3 i)
clc;
clear all;

% Load all the variables
aufgabe6_2_init;

% Check observability
if (rank(obsv(A,C)) == max(size(A)))
    display('The system is fully observable');
else
    display('The system is not fully observable');
end