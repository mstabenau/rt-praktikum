function aufgabe4_1_func( block )
    % Full non-linear system as L2 S-function
    setup(block);
end

function setup(block)
    % Ports
    block.NumInputPorts  = 1;
    block.NumOutputPorts = 2;

    % States
    block.NumContStates = 4;

    % Settings
    block.NumDialogPrms = 2;

    %Input
    block.InputPort(1).Dimensions        = 2;
    block.InputPort(1).SamplingMode = 'Sample';
    block.InputPort(1).DirectFeedthrough = false;
    
    % Output
    block.OutputPort(1).Dimensions       = 4;
    block.OutputPort(1).SamplingMode = 'Sample';
    block.OutputPort(2).Dimensions       = 2;
    block.OutputPort(2).SamplingMode = 'Sample';
    
    % Sample time
    block.SampleTimes = [0 0];
    
    % Callbacks
    block.RegBlockMethod('InitializeConditions',    @InitConditions); 
    block.RegBlockMethod('Outputs',                 @Output);  
    block.RegBlockMethod('Derivatives',             @Derivatives);
end

function InitConditions(block)
    % Initialise with rest state (x0)
    block.ContStates.Data = block.DialogPrm(1).Data;
end

function Output(block)
    % Get states and params
    x = block.ContStates.Data;
    par = block.DialogPrm(2).Data;
    
    % Calculate torques
    M_GSM = par.k_GSM * x(1);
    M_Kopp = par.c_GSMP * x(2) + par.d_GSMP * (x(3)-x(4));
    
    % Output
    block.OutputPort(1).Data = x;
    block.OutputPort(2).Data = [M_GSM,M_Kopp];
end

function Derivatives(block)
    % Inputs
    u_GSM = block.InputPort(1).Data(1);
    M_ext = block.InputPort(1).Data(2);
    
    % States and params
    x = block.ContStates.Data;
    par = block.DialogPrm(2).Data;
  
    block.Derivatives.Data = [
        - (-u_GSM + x(1)*par.R_GSM + par.k_GSM*x(3))/par.L_GSM,
        x(3)-x(4),
        -(par.c_GSMP*x(2) - par.d_GSMP*x(4) + (par.d_vGSM + par.d_GSMP)*x(3) - x(1)*par.k_GSM + par.d_cGSM)/par.J_GSM,
        (par.c_GSMP*x(2) - par.d_qP*x(4)^2 - (par.d_vP + par.d_GSMP)*x(4) + par.d_GSMP*x(3) - M_ext)/par.J_P
    ];
end