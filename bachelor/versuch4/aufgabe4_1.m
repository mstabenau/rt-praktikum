clc
close all
clear all

%% 4.1 a)
% Declare parameters
params.L_GSM     =  100e-6;                 % [H -Henry]    -Ankerkreisinduktivit???t des Motors
params.R_GSM     =  0.825 ;                 % [Ohm]         -Ankerkreiswiderstand des Motors
params.k_GSM     =  0.0413100675;           % [N*m]         -Ankerkreiskonstante des Motors
params.J_GSM     =  5.37e-6;                % eff. Tr??gheitsmoment GSM
params.d_cGSM    =  0.0546464856238636;     % [N*m]         -coulombsche Reibkonstante des Motors (Beschreiben das Reibmoment/den Verlust des Motors)
params.d_vGSM    =  3.48055600677548e-05;   % [N*m*s/rad]   -viskose D???mpfungskonstante des Motors (Beschreiben das Reibmoment/den Verlust des Motors)
params.J_P       =  0.00157782407851527;    % [kg*m^2]      -Massentraegheitsmoment Last
params.d_vP      =  5.91513402436499e-05;   % [N*m*s/rad]   -viskose D???mpfungskonstante Last
params.d_qP      =  5.18976844769572e-08;   % [N*m/rad^2]   -zum Geschwindigkeitsanteil proportionale D???mpfungskonstante Last
params.c_GSMP    =  2.5;                    % [N*m/rad]     -konst. Steifigkeit der Welle
params.d_GSMP    =  0.0388029147241365/10;  % [N*m*s/rad]   -viskose Daempfung der Welle

% Declare state of rest
x0 = [ 0.028271; 269.31; 269.31 ];

% Declare state-space variables
A = [
    0, 1, -1;
    - params.c_GSMP/params.J_GSM, - (params.k_GSM^2/params.R_GSM + params.d_vGSM + params.d_GSMP)/params.J_GSM, params.d_GSMP / params.J_GSM;
    params.c_GSMP/params.J_P, params.d_GSMP/params.J_P, (-2*params.d_qP*x0(3) - params.d_vP - params.d_GSMP)/params.J_P
];

B = [ 0; params.k_GSM/(params.J_GSM * params.R_GSM); 0 ];

C = [ 0 0 1 ];
D = 0;

% Calculate transfer function
system = ss(A,B,C,D);
trans = tf(system);
s = tf('s');

%% 4.1 b)
% Calculate and define target parameters
t_r = 0.2;
ue = 10;
e = 0;

omega_c = 1.5/t_r;
phi = 2*pi/360*(70 - ue);

% Fetch system parameters at omega_c
[mag_system, phi_system] = bode(trans/s, [omega_c]);
phi_system = 2*pi*phi_system/360; % Convert to radiants

% Calculate first control system parameter
T_I = tan( phi + pi - phi_system )/omega_c;

% Get magnitude without V_I
trans_open = (1 + T_I*s)/s*trans;
[ mag_open_loop ] = bode(trans_open, [omega_c]);

% Calculate second parameter
V_I = 1/mag_open_loop;

%% 4.1 c)
trans_controller = V_I * (1 + T_I*s)/s;

figure
bodeplot(trans(1), 'r', trans_controller * trans(1), 'g')
legend('original system', 'open loop controlled system')

%% 4.1 d)
closed_loop = (trans(1) * trans_controller)/(1 + trans(1) * trans_controller);
t = 0:0.01:10;

% Calculate the step responses
h_system = step(trans(1),t);
h_controlled = step(closed_loop,t);

% Plot them
figure
hold on
axis([0 10 -10 10])

plot(t, h_system)
plot(t, h_controlled)
legend('original system', 'controlled system')