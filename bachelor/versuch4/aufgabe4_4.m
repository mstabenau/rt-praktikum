clc

%% 4.4 a)
% State-space variables
A = [ -2 1; 1 1];
B = [ 0; 1 ];
C = [ 1 0 ];
D = 0;

x0  =  [ 3; -0.5 ];
system = ss(A,B,C,D);
n = max(length(A));

% Stability
eig(A) % The second value is positive, so the system is instable

% Check observability
observability = rank(obsv(A,C)) % The system is observable

% Check controllability
controllability = rank(ctrb(A,B)) % The system is controllable

%% 4.4 b)
% Calculate RNF
w_T = [ 0, 1] * ctrb(A,B)^-1;
W = vertcat(w_T, w_T*A);

A_r = W*A*W^-1;
B_r = [ 0; 1 ];
C_r = C * W^-1;
D_r = D;

rnf = ss(A_r, B_r, C_r, D_r);

% Calculate BNF
[ bnf, W_bnf ] = canon(system, 'companion');

%% 4.4 c)
% Target eigenvalues
lambda = [ -2; -1 ];
p = fliplr(poly(lambda)); % Flip to have the same numbers as in the ackerman formula

% i)
p_red = p(1:2); % Remove the last value, which is always 1
k_i = w_T*A^n*W^-1 + p_red;
k_i = k_i*W; % Transform back

% ii)
p = fliplr(poly(lambda)); % Flip to have the same numbers as in the ackerman formula
k_ii = zeros(length(A));

for i = 1:length(p)
    k_ii = k_ii + p(i)*A^(i-1);
end
k_ii = w_T * k_ii;

% iii)
k_iii = place(A,B,lambda);

%% 4.4 d)
% Target observer eigenvalues
lambda_o = [ -8; -4 ];
p_o = fliplr(poly(lambda_o));

% Transformation vector
w_o = obsv(A,C)^-1*[ 0; 1 ];

% i)
p_o_red = p_o(1:2);             % Throw away the 1 at the end
l_i = p_o_red + bnf.a(:,2)';    % Assignment 6.17
l_i = W_bnf^-1*l_i';            % Transform back

% ii)
l_ii = zeros(length(A));

for i = 1:length(p_o)
    l_ii = l_ii + p_o(i)*A^(i-1);
end

l_ii = l_ii * w_o;

% iii)
l_iii = place(A',C',lambda_o)';