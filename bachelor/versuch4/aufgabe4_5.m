%% 4.5 a)
params.L_GSM     =  100e-6;                 % [H -Henry]    -Ankerkreisinduktivit???t des Motors
params.R_GSM     =  0.825 ;                 % [Ohm]         -Ankerkreiswiderstand des Motors
params.k_GSM     =  0.0413100675;           % [N*m]         -Ankerkreiskonstante des Motors
params.J_GSM     =  5.37e-6;                % eff. Tr??gheitsmoment GSM
params.d_cGSM    =  0.0546464856238636;     % [N*m]         -coulombsche Reibkonstante des Motors (Beschreiben das Reibmoment/den Verlust des Motors)
params.d_vGSM    =  3.48055600677548e-05;   % [N*m*s/rad]   -viskose D???mpfungskonstante des Motors (Beschreiben das Reibmoment/den Verlust des Motors)
params.J_P       =  0.00157782407851527;    % [kg*m^2]      -Massentraegheitsmoment Last
params.d_vP      =  5.91513402436499e-05;   % [N*m*s/rad]   -viskose D???mpfungskonstante Last
params.d_qP      =  5.18976844769572e-08;   % [N*m/rad^2]   -zum Geschwindigkeitsanteil proportionale D???mpfungskonstante Last
params.c_GSMP    =  2.5;                    % [N*m/rad]     -konst. Steifigkeit der Welle
params.d_GSMP    =  0.0388029147241365/10;  % [N*m*s/rad]   -viskose Daempfung der Welle

% Declare state of rest
x0 = [ 0.028271; 269.31; 269.31 ];

% Declare state-space variables
A = [
    0, 1, -1;
    - params.c_GSMP/params.J_GSM, - (params.k_GSM^2/params.R_GSM + params.d_vGSM + params.d_GSMP)/params.J_GSM, params.d_GSMP / params.J_GSM;
    params.c_GSMP/params.J_P, params.d_GSMP/params.J_P, (-2*params.d_qP*x0(3) - params.d_vP - params.d_GSMP)/params.J_P
];

B = [ 0; params.k_GSM/(params.J_GSM * params.R_GSM); 0 ];

C = [ 0 0 1 ];
D = 0;

% Create observer
lambda_observer = eig(A);
lambda_observer(3) = -9;
l = place(A',C', lambda_observer)';

%% 4.5 b)
lamba_system = eig(A);
lamba_system(3) = -3;
k = place(A,B, lamba_system);