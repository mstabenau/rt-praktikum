%% 4.3 a)
% Define system parameters
A = [ -2, 1; 1, -1];
B = [ 0; 1];
C = [ 1, 0];
D = 0;

system = ss(A,B,C,D);

% Check stability
eig(A) % Both are negative, so the system is stable

% Check observability
observability = rank(obsv(A,C))

% Check controllability
controllability = rank(ctrb(A,B))

%% 4.3 b)
% Calculate RNF
w_T = [ 0, 1] * ctrb(A,B)^-1;
W = vertcat(w_T, w_T*A);

A_r = W*A*W^-1;
B_r = [ 0; 1 ];
C_r = C * W^-1;
D_r = D;

rnf = ss(A_r, B_r, C_r, D_r);

% Calculate BNF
bnf = canon(system, 'companion');

%% 4.3 c)
figure
t = 0:0.1:30;
hold on

h_original = step(system,t);
h_rnf = step(rnf, t);
h_bnf = step(bnf, t);

plot(t, h_original)
plot(t(1:20:end), h_rnf(1:20:end), 'rx')
plot(t, h_bnf, 'g--')

legend('Original', 'RNF', 'BNF')