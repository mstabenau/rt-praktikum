%% 4.2 Calculates the PI-Controller parameters
function [V_I,T_I] = FKL_GSM(t_r,ue,parameter,x_r)
    % Fetch parameters from array
    params.L_GSM = parameter(1);
    params.R_GSM = parameter(2);
    params.k_GSM = parameter(3);
    params.J_GSM = parameter(4);
    params.d_cGSM = parameter(5);
    params.d_vGSM = parameter(6);
    params.J_P = parameter(7);
    params.d_vP = parameter(8);
    params.d_qP = parameter(9);
    params.c_GSMP = parameter(10);
    params.d_GSMP = parameter(11);
    
    % Declare state-space parameters
    A = [
        - params.R_GSM / params.L_GSM, 0, - params.k_GSM / params.L_GSM, 0;
        0, 0, 1, -1;
        - params.k_GSM / params.J_GSM, - params.c_GSMP/params.J_GSM, - (params.d_vGSM + params.d_GSMP)/params.J_GSM, params.d_GSMP / params.J_GSM;
        0, params.c_GSMP/params.J_P, params.d_GSMP/params.J_P, (-2*params.d_qP*x_r(4) - params.d_vP - params.d_GSMP)/params.J_P
    ];

    B = [
        1/params.L_GSM, 0;
        0, 0;
        0, 0;
        0, - 1/params.J_P
    ];
    
    C = [ 0 0 0 1 ];
    D = 0;
    
    % Get transfer function
    trans = tf(ss(A,B,C,D));
    
    % Calculate target parameters
    omega_c = 1.5/t_r;
    phi = 2*pi/360*(70 - ue);
    
    % Fetch system parameters at omega_c
    [mag_system, phi_system] = bode(trans(1), [omega_c]);
    phi_system = 2*pi*phi_system/360; % Convert to radiants

    % Calculate control system parameters
    T_I = tan( phi - pi - phi_system)/omega_c;
    V_I = (abs(mag_system)*abs((1 + i*omega_c)/(i * omega_c)))^-1;
end