%% Init params
params.s10 = 0.4;
params.s20 = 0.6;
params.m1  = 100;
params.m2  = 1000;
params.c1  = 5000;
params.c2  = 100;
params.d2  = 1000;

%% Init state of rest
F = 0;
x0(1) = F + params.s10;
x0(2) = 0;
x0(3) = F + params.s10 + params.s20;
x0(4) = 0;

%% Init linear system
A = [ 
    0 1 0 0;
    -(params.c1*params.s20 + params.c2*params.s10)/(params.m1*params.s10*params.s20), -params.d2/params.m1, params.c2/(params.m1*params.s20), params.d2/params.m1;
    0 0 0 1;
    params.c2/(params.m2*params.s20), params.d2/params.m2, -params.c2/(params.m2*params.s20), -params.d2/params.m2
];
B = [0; params.c1/(params.m1*params.s10); 0; 0];
C = [0, 0, 1, 0];
D = 0;

linear_sys = ss(A,B,C,D);