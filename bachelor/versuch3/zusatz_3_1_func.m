function aufgabe3_4_func( block )
    % Full non-linear system as L2 S-function
    setup(block);
end

function setup(block)
    % Ports
    block.NumInputPorts  = 1;
    block.NumOutputPorts = 1;

    % States
    block.NumContStates = 4;

    % Settings
    block.NumDialogPrms = 2;

    %Input
    block.InputPort(1).Dimensions        = 1;
    block.InputPort(1).SamplingMode = 'Sample';
    block.InputPort(1).DirectFeedthrough = false;
    
    % Output
    block.OutputPort(1).Dimensions       = 1;
    block.OutputPort(1).SamplingMode = 'Sample';

    % Sample time
    block.SampleTimes = [0 0];
    
    % Callbacks
    block.RegBlockMethod('InitializeConditions',    @InitConditions); 
    block.RegBlockMethod('Outputs',                 @Output);  
    block.RegBlockMethod('Derivatives',             @Derivatives);
end

function InitConditions(block)
    % Initialise with rest state (x0)
    block.ContStates.Data = block.DialogPrm(1).Data;
end

function Output(block)
    % Output s2
    x = block.ContStates.Data;
    block.OutputPort(1).Data = x(3);
end

function Derivatives(block)
    % Inputs
    u = block.InputPort(1).Data(1);

    % States and params
    x = block.ContStates.Data;
    params = block.DialogPrm(2).Data;
    arg1 =  (-u+x(1))/params.s10-1;
    arg2 =  (x(3)-x(1))/params.s20-1;
    dx(1) = x(2);
    dx(2) = ( -params.c1*sinh(arg1) + params.c2*sinh(arg2) + params.d2*(x(4)-x(2)) )/params.m1;
    dx(3) = x(4);
    dx(4) =-( params.c2*sinh(arg2) + params.d2*(x(4)-x(2)))/params.m2;
block.Derivatives.Data = dx;
end