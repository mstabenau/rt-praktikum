function aufgabe3_4_func( block )
    % Full non-linear system as L2 S-function
    setup(block);
end

function setup(block)
    % Ports
    block.NumInputPorts  = 1;
    block.NumOutputPorts = 2;

    % States
    block.NumContStates = 2;

    % Settings
    block.NumDialogPrms = 1;

    %Input
    block.InputPort(1).Dimensions        = 1;
    block.InputPort(1).SamplingMode = 'Sample';
    block.InputPort(1).DirectFeedthrough = false;
    
    % Output
    block.OutputPort(1).Dimensions       = 1;
    block.OutputPort(1).SamplingMode = 'Sample';
    
    block.OutputPort(2).Dimensions       = 1;
    block.OutputPort(2).SamplingMode = 'Sample';

    % Sample time
    block.SampleTimes = [0 0];
    
    % Callbacks
    block.RegBlockMethod('InitializeConditions',    @InitConditions); 
    block.RegBlockMethod('Outputs',                 @Output);  
    block.RegBlockMethod('Derivatives',             @Derivatives);
end

function InitConditions(block)
    % Initialise with rest state (x0)
    block.ContStates.Data = [ 0; 0 ];
end

function Output(block)
    % Output s2
    x = block.ContStates.Data;
    block.OutputPort(1).Data = x(1);
    
    % Output ds2
    block.OutputPort(2).Data = x(2);
end

function Derivatives(block)
    % Inputs
    u = block.InputPort(1).Data(1);

    % States and params
    x = block.ContStates.Data;
    params = block.DialogPrm(1).Data;
    
    % State derivatives
    if ((abs(x(2)) < 0.001) && abs(u-params.k*x(1)) <= params.rh)
        dx(1) = 0;
        dx(2) = 0;
    else
        exp_arg = - (x(2)/params.v0)^2;
        FR = params.rv * x(2) + params.rc * sign(x(2)) + (params.rh-params.rc) * exp(exp_arg)*sign(x(2));
        dx(1) = x(2);
        dx(2) = 1/params.m * (u - FR - params.k*x(1));
    end
    
    % Write derivatives
    block.Derivatives.Data = dx;
end