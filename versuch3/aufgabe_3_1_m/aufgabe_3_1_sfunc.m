function aufgabe_3_1_sfunc(block) 

% level-2 Matlab M-file s-function
%
% -------------------------------------------------------------------------
% Beschreibung: Simulation der Aufgabe 3.1 
% -------------------------------------------------------------------------
%
% inputs:   u nach Methode der exatlen E/A Linearisierung
%
% states:   x(1)
%           x(2)
%           x(3)
%           x(4)
%           x(5)
%
% output = states
%    
% -------------------------------------------------------------------------
% Abtastzeit (sample time): zeitkontinuierlich (continuous)
% -------------------------------------------------------------------------
setup(block);

% -------------------------------------------------------------------------
% Initialisierung des Simulationsobjektes block
% -------------------------------------------------------------------------

function setup(block)
  
  % Anzahl der Eingangs- und Ausgangsports
  block.NumInputPorts  = 1;
  block.NumOutputPorts = 1;
  
  % Anzahl der zeitkontinuierlichen Zustaende
  block.NumContStates = 5;

  % Anzahl der Parameter
  block.NumDialogPrms = 1;
  
  % Dimensionen der Eingangsports
  % Flag DirectFeedthrough kennzeichnet, ob ein Eingang direkt an einem
  % Ausgang auftritt, d.h. y=f(u)
  % Port 1:
  block.InputPort(1).Dimensions        = 2;
  block.InputPort(1).SamplingMode = 'Sample';
  block.InputPort(1).DirectFeedthrough = false;

  % Dimensionen der Ausgangsports  
  % Port 1:
  block.OutputPort(1).Dimensions       = 5;
  block.OutputPort(1).SamplingMode = 'Sample';  
  
  % Einstellen der Abtastzeit: [0 0] wird verwendet fuer die
  % zeitkontinuierliche Simulation.
  block.SampleTimes = [0 0];
  
  % ------------------------------------------------
  % NICHT VERAENDERN
  % ------------------------------------------------
  % 
  % Registrieren der einzelnen Methoden
  % Hier: InitializeConditions ... Initialisierung
  %       Outputs ...       Berechnung der Ausgaenge
  %       Derivatives ...   Berechnung der Zustaende
  %       Terminate ...     Konsistentes Beenden der Simulation

  block.RegBlockMethod('InitializeConditions',    @InitConditions); 
  block.RegBlockMethod('Outputs',                 @Output);  
  block.RegBlockMethod('Derivatives',             @Derivatives);  
  block.RegBlockMethod('Terminate',               @Terminate);


% -------------------------------------------------------------------------
% Setzen der Anfangsbedingungen der Zustaende
% -------------------------------------------------------------------------

function InitConditions(block)

  block.ContStates.Data = block.DialogPrm(1).Data;


% -------------------------------------------------------------------------
% Berechnen der Ausgaenge
% -------------------------------------------------------------------------

function Output(block)

  % Shortcut fuer den Zustand
  block.OutputPort(1).Data = block.ContStates.Data;
 
% -------------------------------------------------------------------------
% Berechnen der Zustaende
% -------------------------------------------------------------------------

function Derivatives(block)
  
  % Shortcut fuer die Zustaende
  x = block.ContStates.Data;
  
   % Shortcut fuer den Eingang
   u = block.InputPort(1).Data;
  
    % Berechnung der Zustaende
  dx(1) = -x(1) + x(2)^2;
  dx(2) = x(1) * x(3) + x(4) + u(2);
  dx(3) = -x(1) + x(3) + u(1);
  dx(4) = x(5) + u(1);
  dx(5) = -x(5) + x(3)^2 + x(2) * u(1);
  
  
  % Schreiben auf Objekt block
  block.Derivatives.Data = dx;


% -------------------------------------------------------------------------
% Operationen am Ende der Simulation
% -------------------------------------------------------------------------

% Die function Terminate wird hier nicht verwendet,
% muss aber vorhanden sein!
function Terminate(block)

