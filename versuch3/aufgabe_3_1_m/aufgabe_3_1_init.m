clc
clear
close all

%% system parameters

par.y_1T = 2;
par.y_2T = 3;
par.T = 10;
par.p1 = 1;
par.p2 = 1;
par.y10 = 0;
par.y20 = 0;

%% initial conditions
x0 = [0;0;0;0;0];