function aufgabe_3_2_sfunc(block)

% level-2 Matlab M-file s-function
%
% -------------------------------------------------------------------------
% Beschreibung: Simulation des Dreitanksystems
% -------------------------------------------------------------------------
%
% inputs:   u 
%
% states:   x(1)
%           x(2)
%           x(3)
%
% output = states
%
% -------------------------------------------------------------------------
% Abtastzeit (sample time): zeitkontinuierlich (continuous)
% -------------------------------------------------------------------------
setup(block);

% -------------------------------------------------------------------------
% Initialisierung des Simulationsobjektes block
% -------------------------------------------------------------------------

function setup(block)

% Anzahl der Eingangs- und Ausgangsports
block.NumInputPorts  = 1;
block.NumOutputPorts = 1;

% Anzahl der zeitkontinuierlichen Zustaende
block.NumContStates = 3;

% Anzahl der Parameter
block.NumDialogPrms = 2;

% Dimensionen der Eingangsports
% Flag DirectFeedthrough kennzeichnet, ob ein Eingang direkt an einem
% Ausgang auftritt, d.h. y=f(u)
% Port 1:
block.InputPort(1).Dimensions        = 2;
block.InputPort(1).SamplingMode = 'Sample';
block.InputPort(1).DirectFeedthrough = false;

% Dimensionen der Ausgangsports
% Port 1:
block.OutputPort(1).Dimensions       = 3;
block.OutputPort(1).SamplingMode = 'Sample';

% Einstellen der Abtastzeit: [0 0] wird verwendet fuer die
% zeitkontinuierliche Simulation.
block.SampleTimes = [0 0];

% ------------------------------------------------
% NICHT VERAENDERN
% ------------------------------------------------
%
% Registrieren der einzelnen Methoden
% Hier: InitializeConditions ... Initialisierung
%       Outputs ...       Berechnung der Ausgaenge
%       Derivatives ...   Berechnung der Zustaende
%       Terminate ...     Konsistentes Beenden der Simulation

block.RegBlockMethod('InitializeConditions',    @InitConditions);
block.RegBlockMethod('Outputs',                 @Output);
block.RegBlockMethod('Derivatives',             @Derivatives);
block.RegBlockMethod('Terminate',               @Terminate);


% -------------------------------------------------------------------------
% Setzen der Anfangsbedingungen der Zustaende
% -------------------------------------------------------------------------

function InitConditions(block)

block.ContStates.Data = block.DialogPrm(1).Data;


% -------------------------------------------------------------------------
% Berechnen der Ausgaenge
% -------------------------------------------------------------------------

function Output(block)

% Shortcut fuer den Zustand
block.OutputPort(1).Data = block.ContStates.Data;

% -------------------------------------------------------------------------
% Berechnen der Zustaende
% -------------------------------------------------------------------------

function Derivatives(block)

% Shortcut fuer die Zustaende
x = block.ContStates.Data;

% Shortcut fuer parameter
par = block.DialogPrm(2).Data;

rv = par.rv;
At = par.At;
a12 = par.a12;
a23 = par.a23;
a10 = par.a10;
a30I = par.a30I;
g = par.g;

% Shortcut fuer den Eingang
u = block.InputPort(1).Data;

% Konstanter Vorfaktor
c = rv^2 * pi * sqrt(2 * g);

% verkürzte Funktionen
q12 = a12 * sign(x(1) - x(2)) * sqrt(abs((x(1) - x(2))));
q23 = a23 * sign(x(2) - x(3)) * sqrt(abs((x(2) - x(3))));

% Berechnung der Zustaende
dx(1) = c/At * ( -a10 * sqrt(x(1)) - q12) + u(1)/At;
dx(2) = c/At * ( q12 - q23 );
dx(3) = c/At * ( -a30I * sqrt(x(3)) + q23) + u(2)/At;


% Schreiben auf Objekt block
block.Derivatives.Data = dx;


% -------------------------------------------------------------------------
% Operationen am Ende der Simulation
% -------------------------------------------------------------------------

% Die function Terminate wird hier nicht verwendet,
% muss aber vorhanden sein!
function Terminate(block)

