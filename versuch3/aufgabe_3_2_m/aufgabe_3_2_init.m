clc
 clear
close all

%% system parameters

par.rv = 0.004;
par.At = 0.0154;
par.a12 = 0.3485;
par.a23 = 0.3485;
par.a10 = 0.6677;
par.a30I = 0.6591;
par.g = 9.80665;

%% initial conditions
x0 = [0.1 0.1 0.1];