clc
clear 
close all

%% Init file for rfj_model
load par.mat;

%% Input parameters
x0      = [0,0,0,0];

%% Linear system parameters
A = [
    0 1 0 0;
    -337.3521240969334 -42.3441319144186 337.3521240969334 0;
    0 0 0 1;
    192.7726423411048 0 -192.7726423411048 -0.042857142857142864
    ];

B = [0; 64.3400930232558; 0; 0];

C = [0, 0, 1, 0];

D = 0;

%% Reference parameters
par.t0 = 2;
par.z0 = x0(3);
par.te = 10;
par.ze = pi;
