clc
clear
close all

load par.mat;


z1 = fzero(@(phi) f1(phi,par),0);
z2 = fzero(@(phi) f2(phi,par),0);

function y1 = f1(phi,par)

K_g     = par.K_g;
eta_g   = par.eta_g;
eta_m   = par.eta_m;
k_b     = par.k_b;
k_m     = par.k_m;
nu      = par.nu;
R_m     = par.R_m;
s0      = par.s0;
r1      = par.r1;
r2      = par.r2;
r3      = par.r3;
k_f     = par.k_f;
d_B     = par.d_B;
d_P     = par.d_P;
J_Pzz   = par.J_Pzz;
J_Bzz   = par.J_Bzz;

alpha=sqrt(r3^2-2*cos(phi)*r1*r3+r2^2+2*sin(phi)*r1*r2+r1^2);
gamma=sqrt(r3^2-2*cos(phi)*r1*r3+r2^2-2*sin(phi)*r1*r2+r1^2);

y1 = -((alpha*k_f*cos(phi)*r1*r2-alpha*k_f*sin(phi)*r1*r3)*R_m*s0+((-k_f*cos(phi)*r1*r2-k_f*sin(phi)*r1*r3)*R_m*s0+2*alpha*k_f*sin(phi)*r1*r3*R_m)*gamma)/(alpha*J_Pzz*R_m*gamma);
end

function y2 = f2(phi,par)

K_g     = par.K_g;
eta_g   = par.eta_g;
eta_m   = par.eta_m;
k_b     = par.k_b;
k_m     = par.k_m;
nu      = par.nu;
R_m     = par.R_m;
s0      = par.s0;
r1      = par.r1;
r2      = par.r2;
r3      = par.r3;
k_f     = par.k_f;
d_B     = par.d_B;
d_P     = par.d_P;
J_Pzz   = par.J_Pzz;
J_Bzz   = par.J_Bzz;

alpha=sqrt(r3^2-2*cos(phi)*r1*r3+r2^2+2*sin(phi)*r1*r2+r1^2);
gamma=sqrt(r3^2-2*cos(phi)*r1*r3+r2^2-2*sin(phi)*r1*r2+r1^2);

y2 = -((alpha*k_f*sin(phi)*r1*r3-alpha*k_f*cos(phi)*r1*r2)*s0+((k_f*cos(phi)*r1*r2+k_f*sin(phi)*r1*r3)*s0-2*alpha*k_f*sin(phi)*r1*r3)*gamma)/(alpha*J_Bzz*gamma);
end

