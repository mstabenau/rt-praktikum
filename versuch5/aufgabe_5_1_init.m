%% Init file for rfj_model
load par.mat;

%% Input parameters
t0      = 1;
tT      = 0.2;
U       = 5;

%% Linear system parameters
[A, B, C] = aufgabe_5_1_lin_ms(par);

%% Reference parameters
par.ref_t0 = 0;
par.ref_te = 5;
par.ref_phi0 = 0;
par.ref_phie = pi/8;
par.beta = 4;