%% Calculate linear state-space system matrices
function [ A, B, C, D ] = aufgabe_5_1_lin_ms( par )
    %% Extract parameters
    K_g     = par.K_g;
    eta_g   = par.eta_g;
    eta_m   = par.eta_m;
    k_b     = par.k_b;
    k_m     = par.k_m;
    nu      = par.nu;
    R_m     = par.R_m;
    s0      = par.s0;
    r1      = par.r1;
    r2      = par.r2;
    r3      = par.r3;
    k_f     = par.k_f;
    d_B     = par.d_B;
    d_P     = par.d_P;
    J_Pzz   = par.J_Pzz;                                                                                                                                                                                                                             
    J_Bzz   = par.J_Bzz;
    
    %% Extract states
    phi_P   = par.xR(1); 
    omega_P = par.xR(2);
    phi_B   = par.xR(3);
    omega_B = par.xR(4);
    
    %% Substituted parameters
    % Copied from Maxima
    alpha = sqrt((-sin(phi_P)*r3-cos(phi_P)*r2+sin(phi_B)*r1)^2+...
        (cos(phi_P)*r3-sin(phi_P)*r2-cos(phi_B)*r1)^2);
    gamma = sqrt((-sin(phi_P)*r3+cos(phi_P)*r2+sin(phi_B)*r1)^2+...
        (cos(phi_P)*r3+sin(phi_P)*r2-cos(phi_B)*r1)^2);
    
    %% Substituted derivation
    % Copied from Maxima
    dalpha_dphi_B = (-sin(phi_P-phi_B)*r1*r3-cos(phi_P-phi_B)*r1*r2)/ ...
        sqrt(r3^2-2*cos(phi_P-phi_B)*r1*r3+r2^2+2*sin(phi_P-phi_B)*...
        r1*r2+r1^2);
    
    dalpha_dphi_P=(sin(phi_P-phi_B)*r1*r3+cos(phi_P-phi_B)*r1*r2)/ ...
        sqrt(r3^2-2*cos(phi_P-phi_B)*r1*r3+r2^2+2*sin(phi_P-phi_B)* ...
        r1*r2+r1^2);
    
    dgamma_dphi_B = (cos(phi_P-phi_B)*r1*r2-sin(phi_P-phi_B)*r1*r3)/ ...
        sqrt(r3^2-2*cos(phi_P-phi_B)*r1*r3+r2^2-2*sin(phi_P-phi_B)*r1*...
        r2+r1^2);
    
    dgamma_dphi_P = (sin(phi_P-phi_B)*r1*r3-cos(phi_P-phi_B)*r1*r2)/ ...
        sqrt(r3^2-2*cos(phi_P-phi_B)*r1*r3+r2^2-2*sin(phi_P-phi_B)*r1*...
        r2+r1^2);
    
    %% Calculate parameters
    % Copied from Maxima
    A = zeros(4,4);
    numerator_2_1 = (((k_f*(dalpha_dphi_P*sin(phi_P-phi_B)-alpha* ...
        cos(phi_P-phi_B))*r1*r3+k_f*(alpha*sin(phi_P-phi_B)+ ...
        dalpha_dphi_P*cos(phi_P-phi_B))*r1*r2)*s0+2*alpha^2*k_f*...
        cos(phi_P-phi_B)*r1*r3)*gamma^2+(-alpha^2*k_f*cos(phi_P-phi_B)*...
        r1*r3-alpha^2*k_f*sin(phi_P-phi_B)*r1*r2)*s0*gamma+...
        dgamma_dphi_P*(alpha^2*k_f*sin(phi_P-phi_B)*r1*r3-alpha^2*k_f*...
        cos(phi_P-phi_B)*r1*r2)*s0);

    A(2,1) = -numerator_2_1 / J_Pzz*alpha^2*gamma^2;
    
    A(4, 1) = numerator_2_1 / J_Bzz*alpha^2*gamma^2;

    A(1, 2) = 1;
    
    A(2, 2) = -(k_m*nu+R_m*d_P)/(J_Pzz*R_m);
    
    
    numerator_2_3 = -(((k_f*(dalpha_dphi_B*sin(phi_P-phi_B)+alpha*...
        cos(phi_P-phi_B))*r1*r3+k_f*(dalpha_dphi_B*cos(phi_P-phi_B)-...
        alpha*sin(phi_P-phi_B))*r1*r2)*s0-2*alpha^2*k_f*cos(phi_P-phi_B)*...
        r1*r3)*gamma^2+(alpha^2*k_f*cos(phi_P-phi_B)*r1*r3+alpha^2*k_f*...
        sin(phi_P-phi_B)*r1*r2)*s0*gamma+(alpha^2*dgamma_dphi_B*k_f*...
        sin(phi_P-phi_B)*r1*r3-alpha^2*dgamma_dphi_B*k_f*...
        cos(phi_P-phi_B)*r1*r2)*s0);
    
    A(2, 3) = - numerator_2_3 / (J_Pzz*alpha^2*gamma^2);
    
    A(4, 3) = numerator_2_3 / (J_Bzz*alpha^2*gamma^2);
    
    A(3, 4) = 1;
    
    A(4, 4) = -d_B/J_Bzz;

    B = [0; 0; nu/(J_Pzz*R_m); 0];
    C = [0, 0, 1, 0];
end