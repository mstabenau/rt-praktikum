function aufgabe_2_7_sfunc(block)

% level-2 Matlab M-file s-function
%
% -------------------------------------------------------------------------
% Beschreibung: Simulation des Turmdrehkrans
% -------------------------------------------------------------------------
%
% inputs:   u = a_W              Wagenbeschleunigung
%
% states:   x(1)... y          Position des Wagens
%           x(2)... yp          Geschwindigkeit des Wagens
%           x(3)... phi1       Winkel Pendelarm 1
%           x(4)... phi1p       Winkelgeschwindigkeit Pendelarm 1
%           x(5)... phi2       Winkel Pendelarm 2
%           x(6)... phi2p     Winkelgeschwindigkeit Pendelarm 2
%
% output = states
%
% -------------------------------------------------------------------------
% Abtastzeit (sample time): zeitkontinuierlich (continuous)
% -------------------------------------------------------------------------
setup(block);

% -------------------------------------------------------------------------
% Initialisierung des Simulationsobjektes block
% -------------------------------------------------------------------------

function setup(block)

% Anzahl der Eingangs- und Ausgangsports
block.NumInputPorts  = 2;
block.NumOutputPorts = 1;

% Anzahl der zeitkontinuierlichen Zustaende
% state 1:6 -> x
% state 7:42 -> P
block.NumContStates = 42;

% Anzahl der Parameter
% xk0, P0, Q, R, par
block.NumDialogPrms = 5;

% y
block.InputPort(1).Dimensions        = 1;
block.InputPort(1).SamplingMode = 'Sample';
block.InputPort(1).DirectFeedthrough = false;

% u
block.InputPort(2).Dimensions         = 3;
block.InputPort(2).SamplingMode       = 'Sample';
block.InputPort(2).DirectFeedthrough  = false;

% Dimensionen der Ausgangsports
% Port 1:
block.OutputPort(1).Dimensions       = 6;
block.OutputPort(1).SamplingMode = 'Sample';

% Einstellen der Abtastzeit: [0 0] wird verwendet fuer die
% zeitkontinuierliche Simulation.
block.SampleTimes = [0 0];

% ------------------------------------------------
% NICHT VERAENDERN
% ------------------------------------------------
%
% Registrieren der einzelnen Methoden
% Hier: InitializeConditions ... Initialisierung
%       Outputs ...       Berechnung der Ausgaenge
%       Derivatives ...   Berechnung der Zustaende
%       Terminate ...     Konsistentes Beenden der Simulation

block.RegBlockMethod('InitializeConditions',    @InitConditions);
block.RegBlockMethod('Outputs',                 @Output);
block.RegBlockMethod('Derivatives',             @Derivatives);
block.RegBlockMethod('Terminate',               @Terminate);


% -------------------------------------------------------------------------
% Setzen der Anfangsbedingungen der Zustaende
% -------------------------------------------------------------------------

function InitConditions(block)

block.ContStates.Data(1:6) = block.DialogPrm(1).Data;
block.ContStates.Data(7:42) = block.DialogPrm(2).Data(:);


% -------------------------------------------------------------------------
% Berechnen der Ausgaenge
% -------------------------------------------------------------------------

function Output(block)

% Shortcut fuer den Zustand
block.OutputPort(1).Data = block.ContStates.Data(1:6);



% -------------------------------------------------------------------------
% Berechnen der Zustaende
% -------------------------------------------------------------------------

function Derivatives(block)

% Einlesen der Modellparameter (Parameter-Port 2)
par = block.DialogPrm(5).Data;

l1 = par.l1;
l_S1 = par.l_S1;
m_G1 = par.m_G1;
J_G1xx = par.J_G1xx;
k_G1 = par.k_G1;
l2 = par.l2;
l_S2 = par.l_S2;
m_G2 = par.m_G2;
J_G2xx = par.J_G2xx;
k_G2 = par.k_G2;
g = par.g;

% Shortcut fuer den Eingang
a_W = block.InputPort(1).Data;

% Shortcut fuer die Zustaende
x = block.ContStates.Data;

% Shortcut fuer Eingang
measure = block.InputPort(2).Data;

% Ricatti-Matrix
P = reshape(x(7:42), 6, 6);

% Kovarianzmatrix
Q = block.DialogPrm(3).Data;
R = block.DialogPrm(4).Data;

% Substitutionen
y=x(1);
yp=x(2);
phi1=x(3);
phi1p=x(4);
phi2=x(5);
phi2p=x(6);

% Berechnung der Zustaende
dx(1) = yp;
dx(2) = a_W;
dx(3) = phi1p;
dx4num = -(-J_G2xx*omega_1*k_G1+(J_G2xx*omega_2-J_G2xx*omega_1)*k_G2+(J_G2xx*cos(phi_1)*a_W+g*J_G2xx*sin(phi_1))*l_S1*m_G1+((g*sin(phi_1)+cos(phi_1)*a_W)*l_S1*l_S2^2*m_G1+((omega_2-omega_1)*k_G2-omega_1*k_G1)*l_S2^2+(J_G2xx*l1*omega_2^2*sin(phi_2-phi_1)+(l1*omega_2-l1*omega_1)*cos(phi_2-phi_1)*k_G2)*l_S2+J_G2xx*l1*cos(phi_1)*a_W+g*J_G2xx*l1*sin(phi_1))*m_G2+(l1*omega_2^2*sin(phi_2-phi_1)*l_S2^3+(g*l1*sin(phi_1)-g*l1*sin(phi_2)*cos(phi_2-phi_1)+l1^2*omega_1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)+(l1*cos(phi_1)-l1*cos(phi_2)*cos(phi_2-phi_1))*a_W)*l_S2^2)*m_G2^2);
dx4denum = ((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
dx(4) = dx4num/dx4denum;
dx(5) = phi2p;
dx6num = ((J_G1xx*omega_2-J_G1xx*omega_1)*k_G2+(omega_2-omega_1)*k_G2*l_S1^2*m_G1+(((l1*cos(phi_1)*cos(phi_2-phi_1)*a_W+g*l1*sin(phi_1)*cos(phi_2-phi_1))*l_S1+(-cos(phi_2)*a_W+l1*omega_1^2*sin(phi_2-phi_1)-g*sin(phi_2))*l_S1^2)*l_S2*m_G1+(-g*J_G1xx*sin(phi_2)+J_G1xx*l1*omega_1^2*sin(phi_2-phi_1)-J_G1xx*cos(phi_2)*a_W-l1*omega_1*cos(phi_2-phi_1)*k_G1+(l1*omega_2-l1*omega_1)*cos(phi_2-phi_1)*k_G2)*l_S2+(l1^2*omega_2-l1^2*omega_1)*k_G2)*m_G2+(l1^2*omega_2^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2+(-g*l1^2*sin(phi_2)+g*l1^2*sin(phi_1)*cos(phi_2-phi_1)+l1^3*omega_1^2*sin(phi_2-phi_1)+(l1^2*cos(phi_1)*cos(phi_2-phi_1)-l1^2*cos(phi_2))*a_W)*l_S2)*m_G2^2);
dx6denum = ((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
dx(6) = dx6num/dx6denum;

% Ein und Ausgangs Jakobi-Matrizen
H = [
    1 0 0 0 0 0;
    0 0 1 0 0 0;
    0 0 0 0 1 0
    ];

F = zeros(6,6);
% Copied from maxima
F(1,2) = 1;
F(4,3) = (2*l1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2*m_G2^2*(-J_G2xx*omega_1*k_G1+(J_G2xx*omega_2-J_G2xx*omega_1)*k_G2+(J_G2xx*cos(phi_1)*a_W+g*J_G2xx*sin(phi_1))*l_S1*m_G1+((g*sin(phi_1)+cos(phi_1)*a_W)*l_S1*l_S2^2*m_G1+((omega_2-omega_1)*k_G2-omega_1*k_G1)*l_S2^2+(J_G2xx*l1*omega_2^2*sin(phi_2-phi_1)+(l1*omega_2-l1*omega_1)*cos(phi_2-phi_1)*k_G2)*l_S2+J_G2xx*l1*cos(phi_1)*a_W+g*J_G2xx*l1*sin(phi_1))*m_G2+(l1*omega_2^2*sin(phi_2-phi_1)*l_S2^3+(g*l1*sin(phi_1)-g*l1*sin(phi_2)*cos(phi_2-phi_1)+l1^2*omega_1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)+(l1*cos(phi_1)-l1*cos(phi_2)*cos(phi_2-phi_1))*a_W)*l_S2^2)*m_G2^2))/(-J_G1xx*J_G2xx-J_G2xx*l_S1^2*m_G1+(-l_S1^2*l_S2^2*m_G1-J_G1xx*l_S2^2-J_G2xx*l1^2)*m_G2+(l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2)^2-((g*J_G2xx*cos(phi_1)-J_G2xx*sin(phi_1)*a_W)*l_S1*m_G1+((g*cos(phi_1)-sin(phi_1)*a_W)*l_S1*l_S2^2*m_G1+((l1*omega_2-l1*omega_1)*sin(phi_2-phi_1)*k_G2-J_G2xx*l1*omega_2^2*cos(phi_2-phi_1))*l_S2-J_G2xx*l1*sin(phi_1)*a_W+g*J_G2xx*l1*cos(phi_1))*m_G2+((g*l1*cos(phi_1)-l1^2*omega_1^2*cos(phi_2-phi_1)^2-g*l1*sin(phi_2)*sin(phi_2-phi_1)+l1^2*omega_1^2*sin(phi_2-phi_1)^2+(-l1*cos(phi_2)*sin(phi_2-phi_1)-l1*sin(phi_1))*a_W)*l_S2^2-l1*omega_2^2*cos(phi_2-phi_1)*l_S2^3)*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
F(6,3) = ((((l1*cos(phi_1)*sin(phi_2-phi_1)*a_W-l1*sin(phi_1)*cos(phi_2-phi_1)*a_W+g*l1*sin(phi_1)*sin(phi_2-phi_1)+g*l1*cos(phi_1)*cos(phi_2-phi_1))*l_S1-l1*omega_1^2*cos(phi_2-phi_1)*l_S1^2)*l_S2*m_G1+(-J_G1xx*l1*omega_1^2*cos(phi_2-phi_1)-l1*omega_1*sin(phi_2-phi_1)*k_G1+(l1*omega_2-l1*omega_1)*sin(phi_2-phi_1)*k_G2)*l_S2)*m_G2+(l1^2*omega_2^2*sin(phi_2-phi_1)^2*l_S2^2-l1^2*omega_2^2*cos(phi_2-phi_1)^2*l_S2^2+(-l1^3*omega_1^2*cos(phi_2-phi_1)+g*l1^2*cos(phi_1)*cos(phi_2-phi_1)+g*l1^2*sin(phi_1)*sin(phi_2-phi_1)+(l1^2*cos(phi_1)*sin(phi_2-phi_1)-l1^2*sin(phi_1)*cos(phi_2-phi_1))*a_W)*l_S2)*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx)-(2*l1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2*m_G2^2*((J_G1xx*omega_2-J_G1xx*omega_1)*k_G2+(omega_2-omega_1)*k_G2*l_S1^2*m_G1+(((l1*cos(phi_1)*cos(phi_2-phi_1)*a_W+g*l1*sin(phi_1)*cos(phi_2-phi_1))*l_S1+(-cos(phi_2)*a_W+l1*omega_1^2*sin(phi_2-phi_1)-g*sin(phi_2))*l_S1^2)*l_S2*m_G1+(-g*J_G1xx*sin(phi_2)+J_G1xx*l1*omega_1^2*sin(phi_2-phi_1)-J_G1xx*cos(phi_2)*a_W-l1*omega_1*cos(phi_2-phi_1)*k_G1+(l1*omega_2-l1*omega_1)*cos(phi_2-phi_1)*k_G2)*l_S2+(l1^2*omega_2-l1^2*omega_1)*k_G2)*m_G2+(l1^2*omega_2^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2+(-g*l1^2*sin(phi_2)+g*l1^2*sin(phi_1)*cos(phi_2-phi_1)+l1^3*omega_1^2*sin(phi_2-phi_1)+(l1^2*cos(phi_1)*cos(phi_2-phi_1)-l1^2*cos(phi_2))*a_W)*l_S2)*m_G2^2))/(-J_G1xx*J_G2xx-J_G2xx*l_S1^2*m_G1+(-l_S1^2*l_S2^2*m_G1-J_G1xx*l_S2^2-J_G2xx*l1^2)*m_G2+(l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2)^2;
F(3, 4) = 1;
F(4, 4) = -(-J_G2xx*k_G1-J_G2xx*k_G2+((-k_G1-k_G2)*l_S2^2-l1*cos(phi_2-phi_1)*k_G2*l_S2)*m_G2+2*l1^2*omega_1*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
F(6, 4) = (-J_G1xx*k_G2-k_G2*l_S1^2*m_G1+(2*l1*omega_1*sin(phi_2-phi_1)*l_S1^2*l_S2*m_G1+(2*J_G1xx*l1*omega_1*sin(phi_2-phi_1)-l1*cos(phi_2-phi_1)*k_G1-l1*cos(phi_2-phi_1)*k_G2)*l_S2-l1^2*k_G2)*m_G2+2*l1^3*omega_1*sin(phi_2-phi_1)*l_S2*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
F(4, 5) = (2*l1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2*m_G2^2*(-J_G2xx*omega_1*k_G1+(J_G2xx*omega_2-J_G2xx*omega_1)*k_G2+(J_G2xx*cos(phi_1)*a_W+g*J_G2xx*sin(phi_1))*l_S1*m_G1+((g*sin(phi_1)+cos(phi_1)*a_W)*l_S1*l_S2^2*m_G1+((omega_2-omega_1)*k_G2-omega_1*k_G1)*l_S2^2+(J_G2xx*l1*omega_2^2*sin(phi_2-phi_1)+(l1*omega_2-l1*omega_1)*cos(phi_2-phi_1)*k_G2)*l_S2+J_G2xx*l1*cos(phi_1)*a_W+g*J_G2xx*l1*sin(phi_1))*m_G2+(l1*omega_2^2*sin(phi_2-phi_1)*l_S2^3+(g*l1*sin(phi_1)-g*l1*sin(phi_2)*cos(phi_2-phi_1)+l1^2*omega_1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)+(l1*cos(phi_1)-l1*cos(phi_2)*cos(phi_2-phi_1))*a_W)*l_S2^2)*m_G2^2))/(-J_G1xx*J_G2xx-J_G2xx*l_S1^2*m_G1+(-l_S1^2*l_S2^2*m_G1-J_G1xx*l_S2^2-J_G2xx*l1^2)*m_G2+(l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2)^2-((J_G2xx*l1*omega_2^2*cos(phi_2-phi_1)-(l1*omega_2-l1*omega_1)*sin(phi_2-phi_1)*k_G2)*l_S2*m_G2+(l1*omega_2^2*cos(phi_2-phi_1)*l_S2^3+(-g*l1*cos(phi_2)*cos(phi_2-phi_1)+l1^2*omega_1^2*cos(phi_2-phi_1)^2+g*l1*sin(phi_2)*sin(phi_2-phi_1)-l1^2*omega_1^2*sin(phi_2-phi_1)^2+(l1*cos(phi_2)*sin(phi_2-phi_1)+l1*sin(phi_2)*cos(phi_2-phi_1))*a_W)*l_S2^2)*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
F(6, 5) = ((((-l1*cos(phi_1)*sin(phi_2-phi_1)*a_W-g*l1*sin(phi_1)*sin(phi_2-phi_1))*l_S1+(sin(phi_2)*a_W+l1*omega_1^2*cos(phi_2-phi_1)-g*cos(phi_2))*l_S1^2)*l_S2*m_G1+(-g*J_G1xx*cos(phi_2)+J_G1xx*l1*omega_1^2*cos(phi_2-phi_1)+J_G1xx*sin(phi_2)*a_W+l1*omega_1*sin(phi_2-phi_1)*k_G1-(l1*omega_2-l1*omega_1)*sin(phi_2-phi_1)*k_G2)*l_S2)*m_G2+(-l1^2*omega_2^2*sin(phi_2-phi_1)^2*l_S2^2+l1^2*omega_2^2*cos(phi_2-phi_1)^2*l_S2^2+(-g*l1^2*cos(phi_2)+l1^3*omega_1^2*cos(phi_2-phi_1)-g*l1^2*sin(phi_1)*sin(phi_2-phi_1)+(l1^2*sin(phi_2)-l1^2*cos(phi_1)*sin(phi_2-phi_1))*a_W)*l_S2)*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx)+(2*l1^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2*m_G2^2*((J_G1xx*omega_2-J_G1xx*omega_1)*k_G2+(omega_2-omega_1)*k_G2*l_S1^2*m_G1+(((l1*cos(phi_1)*cos(phi_2-phi_1)*a_W+g*l1*sin(phi_1)*cos(phi_2-phi_1))*l_S1+(-cos(phi_2)*a_W+l1*omega_1^2*sin(phi_2-phi_1)-g*sin(phi_2))*l_S1^2)*l_S2*m_G1+(-g*J_G1xx*sin(phi_2)+J_G1xx*l1*omega_1^2*sin(phi_2-phi_1)-J_G1xx*cos(phi_2)*a_W-l1*omega_1*cos(phi_2-phi_1)*k_G1+(l1*omega_2-l1*omega_1)*cos(phi_2-phi_1)*k_G2)*l_S2+(l1^2*omega_2-l1^2*omega_1)*k_G2)*m_G2+(l1^2*omega_2^2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2+(-g*l1^2*sin(phi_2)+g*l1^2*sin(phi_1)*cos(phi_2-phi_1)+l1^3*omega_1^2*sin(phi_2-phi_1)+(l1^2*cos(phi_1)*cos(phi_2-phi_1)-l1^2*cos(phi_2))*a_W)*l_S2)*m_G2^2))/(-J_G1xx*J_G2xx-J_G2xx*l_S1^2*m_G1+(-l_S1^2*l_S2^2*m_G1-J_G1xx*l_S2^2-J_G2xx*l1^2)*m_G2+(l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2)^2;
F(4, 6) = -(J_G2xx*k_G2+(k_G2*l_S2^2+(2*J_G2xx*l1*omega_2*sin(phi_2-phi_1)+l1*cos(phi_2-phi_1)*k_G2)*l_S2)*m_G2+2*l1*omega_2*sin(phi_2-phi_1)*l_S2^3*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);
F(5, 6) = 1;
F(6,6) = (J_G1xx*k_G2+k_G2*l_S1^2*m_G1+(l1*cos(phi_2-phi_1)*k_G2*l_S2+l1^2*k_G2)*m_G2+2*l1^2*omega_2*cos(phi_2-phi_1)*sin(phi_2-phi_1)*l_S2^2*m_G2^2)/((l1^2*cos(phi_2-phi_1)^2-l1^2)*l_S2^2*m_G2^2+(-J_G2xx*l1^2-J_G1xx*l_S2^2-l_S1^2*l_S2^2*m_G1)*m_G2-J_G2xx*l_S1^2*m_G1-J_G1xx*J_G2xx);


% Ricatti Matrix
R_inv = inv(R);
Pdot = F*P + P*F' + Q - P*H'*R_inv*H*P; %#ok<MINV>
dx(7:42) = Pdot(:);

% Verstärkung
L = P * H' * R_inv;

% output
dx(1:6) = dx(1:6) + (L * (measure - [x(1); x(3); x(5)]))';

% Schreiben auf Objekt block
block.Derivatives.Data = dx;

% -------------------------------------------------------------------------
% Operationen am Ende der Simulation
% -------------------------------------------------------------------------

% Die function Terminate wird hier nicht verwendet,
% muss aber vorhanden sein!
function Terminate(block)

