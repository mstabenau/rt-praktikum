%% Aufgabe 2.3

A = [2 1 -1;
     7 5 2;
     13 -8 5];

b = [1 -4 15]';

% Direct solution through inverting matrix A and multiplying with vector b.
% This should be used, when all the elements of the inverted matrix need to be
% known. For a system of linear equations, this might be slower.
x_sol1 = inv(A)*b

% Solve systems of linear equations instead of inverting the matrix A. Smarter
% algorithms are applied to avoid calculating every element of A^-1.
x_sol2 = mldivide(A,b) 
x_sol2 = A\b
